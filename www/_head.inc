<!-- Global site tag (gtag.js) - Google Analytics -->
<script async data-cookieyes="cookieyes-analytics" src="https://www.googletagmanager.com/gtag/js?id=UA-47076740-2"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-47076740-2');
</script>


<!-- Start cookieyes banner -->
<script id="cookieyes" type="text/javascript" src="https://cdn-cookieyes.com/client_data/965b06f93e761faa105251be.js"></script>
<!-- End cookieyes banner -->
