<?php

$Customer_Name                  = aes_encrypt($_POST['Customer_Name']); //Aesencrypted
$Customer_EmailId               = aes_encrypt($_POST['Customer_EmailId']); //Aesencrypted
$Customer_Mobile                = aes_encrypt($_POST['Customer_Mobile']); //Aesencrypted
$Customer_AccountNo             = aes_encrypt($_POST['Customer_AccountNo']); //Aesencrypted
$Customer_StartDate             = $_POST['Customer_StartDate'];
$Customer_ExpiryDate            = $_POST['Customer_ExpiryDate'];
$Customer_DebitAmount           = number_format($_POST['Customer_DebitAmount'], 2);
$Customer_MaxAmount             = 0;
$Customer_DebitFrequency        = $_POST['Customer_DebitFrequency'];

if ($Customer_DebitFrequency == 'ADHO') {
    $Customer_SequenceType = 'OOFF';
    $Customer_DebitFrequency = '';
} else {
    $Customer_SequenceType      = 'RCUR';
}

$Customer_InstructedMemberId    = $_POST['Customer_InstructedMemberId'];
$Customer_Reference1            = aes_encrypt($_POST['Customer_Reference1']); //Aesencrypted
$Customer_Reference2            = aes_encrypt($CustomerRef2); //Aesencrypted
$Channel                        = $_POST['Channel'];
$Filler5                        = $_POST['Filler5'];
$Filler6                        = $_POST['Filler6'];
$Filler9                        = '';
$Filler10                       = '';

$error         = isset($_POST['error']) ? $_POST['error'] : '';
$error_Message = isset($_POST['error_Message']) ? $_POST['error_Message'] : '';
