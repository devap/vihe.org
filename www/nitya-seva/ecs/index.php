<?php
include '_0-EnvSetup.php';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <title>VIHE ECS Mandate</title>
    <!--# include virtual="/payment/_head.inc" -->
    <style type="text/css">
        input[type="text"] {
            display: block;
        }
    </style>
</head>

<body>
    <div id="wrapper">
        <!--# include virtual="/_header.inc" -->
        <!-- start_page -->
        <div class="page">

            <div class="center-well-ecsmandate">

                <div class="page_title" align="center">
                    Vrindavan Institute for Higher Education<br />
                    <small>

                        ECS Mandate Application Form
                    </small>
                </div>

                <?php if (strcasecmp($_SERVER['REQUEST_METHOD'], 'GET') == 0) { ?>

                    <div class="container-fluid">

                        <div class="form-check-inline">
                            <h3>Terms &amp; Conditions</h3>

                            <div class="terms-conditions">
                                <ol>
                                    <li>
                                        I agree for the debit of mandate Processing charges by the bank whom I am authorizing to debit my account as per latest schedule of charges of the bank.
                                    </li>
                                    <li>
                                        This is to confirm that the declaration has been carefully read and understood by me/us. I am authorizing the user entity / Corporate to debit my account based on the instructions as agreed and signed by me.
                                    </li>
                                    <li>
                                        I have understood that I am authorized to cancel/amend this mandate by appropriately communicating the cancellation/ amendment request to the user entity / corporate or the bank where I have authorized the debit.
                                    </li>
                                    <li>
                                        I/We hereby declare that the above information is true and correct and that the mobile number listed above is registered in my our name (s) and/or is the number that I/We use in the ordinary course. I/We declare that, irrespective of my/our registration of the above mobile in the provider customer preference register, or in any similar register maintained under applicable laws or subsequent to the date hereof, I/We consent to the Bank communicating to me/us about the transaction carried out in my/our aforesaid account(s).
                                    </li>
                                    <li>
                                        I/We understand that I/We can find HDFC Bank's privacy notice hosted on the Bank website.
                                    </li>

                                </ol>
                                <p>
                                    <br />Accept Terms &amp; conditions?:
                                </p>
                            </div>

                            <div class="checkbox">
                                <label>
                                    <input type="radio" name="tc" value="no" checked="checked"> No
                                </label>
                                <label>
                                    <input type="radio" name="tc" value="yes"> Yes
                                </label>
                            </div>
                        </div>

                        <form id="ecsmandate-form" action="" method="POST">

                            <div class="section-person bg-info">
                                <div class="row row-name">
                                    <div class="form-group form-group-sm col-xs-5">
                                        <label control-label for="Customer_Name">First Name</label>
                                        <input type="text" class="form-control" id="Customer_Name" name="Customer_Name" placeholder="" required />
                                    </div>
                                    <div class="form-group form-group-sm col-xs-5">
                                        <label control-label for="Customer_Mobile">Phone</label><br />
                                        <input type="tel" id="Customer_Mobile" name="Customer_Mobile" class="form-control" maxlength="34" required />
                                        <br />
                                        <small id="emailHelp" class="form-text text-muted">
                                            (must contain chars 0 to 9 only).
                                        </small>
                                        <br />
                                        <div id="helpTxtPhone" class="help-block">Please enter Phone #</div>
                                    </div>
                                </div>

                                <div class="row row-email">
                                    <div class="form-group form-group-sm col-xs-5">
                                        <label control-label for="Customer_EmailId">Email address</label>
                                        <input type="email" class="form-control" id="Customer_EmailId" name="Customer_EmailId" aria-describedby="emailHelp" maxlength="50" required />
                                    </div>

                                </div>

                            </div><!-- /.section-person -->

                            <div class="section-bankinfo">
                                <div class="row row-acct-ifsc">
                                    <div class="form-group form-group-sm col-xs-5" style="padding-top:10px;">
                                        <label control-label for="Filler6">Bank Code</label>
                                        <div id="Filler6" class="bfh-selectbox" data-name="Filler6" data-filter="true">
                                            <div data-value=""></div>
                                            <?php include '_get_bank_codes.php'; ?>
                                        </div>
                                        <div id="helpTxtFiller6" class="help-block">
                                            Please select your Bank from the list.
                                        </div>
                                    </div>
                                    <!-- Filler6
                                     Bank Code. As per circular, you will have to provide an option to customer to select the Bank from list of active banks with e-Mandate option. You will have to sync your Bank master with NPCI Bank Master and as per customer selection send respective bank code to e-Mandate application.
                                     -->
                                    <div class="form-group form-group-sm col-xs-5" style="padding-top:10px;">
                                        <label control-label for="Customer_AccountNo">IFSC</label>
                                        <input type="text"
                                               class="form-control"
                                               id="Customer_InstructedMemberId"
                                               name="Customer_InstructedMemberId"
                                               required />
                                    </div>
                                </div>
                                <div class="row row-acct-customer">
                                    <div class="form-group form-group-sm col-xs-5" style="padding-top:10px;">
                                        <label control-label for="Customer_Reference1">Reference</label>
                                        <input type="text" class="form-control" id="Customer_Reference1" name="Customer_Reference1" />

                                    </div>
                                    <div class="form-group form-group-sm col-xs-5" style="padding-top:10px;">
                                        <label control-label>Debit Type</label>
                                        <div id="radio_debit_type">
                                            <label>
                                                <input type="radio" id="radio_debit" name="Channel" value="Debit"> Debit Card
                                            </label>
                                            <label>
                                                <input type="radio" id="radio_net" name="Channel" value="Net"> Net Banking
                                            </label>
                                            <div id="helpTxtDebitType" class="help-block">
                                                Please select the Debit type.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="section-acctinfo">
                                <div class="row row-acctid">
                                    <div class="form-group form-group-sm col-xs-10" style="padding-top:10px;">
                                        <label control-label for="Customer_AccountNo">Account Number</label>
                                        <input type="text" class="form-control" id="Customer_AccountNo" name="Customer_AccountNo" required />
                                    </div>
                                </div>

                                <div class="row row-accttype">
                                    <div class="form-group form-group-sm col-xs-5">
                                        <label control-label for="Filler5">Account Type</label>
                                        <div class="bfh-selectbox" data-name="Filler5">
                                            <div data-value=""></div>
                                            <div data-value="S">Savings</div>
                                            <div data-value="C">Current</div>
                                            <div data-value="O">Other</div>
                                        </div>
                                        <div id="helpTxtFiller5" class="help-block">
                                            Please select the Account Type.
                                        </div>
                                    </div>

                                </div>

                                <div class="row row-dateinfo">
                                    <div class="form-group form-group-sm col-xs-5">
                                        <label control-label for="StartDate">Start Date</label>
                                        <div id="StartDate" class="bfh-datepicker" data-format="y-m-d" data-name="Customer_StartDate" data-min="today"></div>
                                    </div>
                                    <div class=" form-group form-group-sm col-xs-5">
                                        <label control-label for="EndDate">End Date</label>
                                        <div id="EndDate" class="bfh-datepicker" data-format="y-m-d" data-name="Customer_ExpiryDate" data-min="today" data-date=""></div>
                                        <div id="helpTxtCustomer_ExpiryDate" class="help-block">
                                            Please select the End Date.
                                        </div>
                                    </div>
                                </div>

                                <div class="row row-debitinfo">
                                    <div class="form-group form-group-sm col-xs-5">
                                        <label control-label for="Customer_DebitAmount">Debit Amount &#x20B9;</label>
                                        <input type="number" class="form-control" id="Customer_DebitAmount" name="Customer_DebitAmount" placeholder="&#x20B9;" required />
                                        <div id=" helpTxtCustomer_DebitAmount" class="help-block">
                                            Please provide a Debit Amount.
                                        </div>
                                    </div>

                                    <div class="form-group form-group-sm col-xs-5">
                                        <label control-label for="Customer_DebitFrequency">Debit Frequency</label>
                                        <div id="DebitFrequency" class="bfh-selectbox" data-name="Customer_DebitFrequency">
                                            <div data-value=""></div>
                                            <div data-value="ADHO">One-Time</div>
                                            <div data-value="DAIL">Daily</div>
                                            <div data-value="WEEK">Weekly</div>
                                            <div data-value="MNTH">Monthly</div>
                                            <div data-value="BIMN">Bi-Monthly</div>
                                            <div data-value="QURT">Quarterly</div>
                                            <div data-value="MIAN">Half yearly</div>
                                            <div data-value="YEAR">Yearly</div>
                                        </div>
                                        <div id="helpTxtCustomer_DebitFrequency" class="help-block">
                                            Please select the debit frequency.
                                        </div>
                                    </div>
                                </div>
                            </div> <!-- /.section-bankinfo -->


                            <button id="submit_form" type="submit" class="btn btn-info">Submit</button>

                        </form>
                    </div>

                <?php } //end if GET
                echo $html; //submit request
                ?>

            </div><!-- /.center-well-ecsmandate -->

            <!--# include virtual="/_footer.inc" -->

        </div><!-- / #page -->
    </div><!-- / #wrapper -->
    <!--# include virtual="/payment/_bottom.inc" -->
    <script src="/js/jquery-dateFormat.min.js"></script>
    <!--# include virtual="/nitya-seva/ecs/_js_formval.html" -->
</body>

</html>
