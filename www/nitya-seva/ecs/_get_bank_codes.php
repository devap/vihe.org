<?php
    include 'dbConfig.php';

    mysqli_select_db($conn, $database);
    $sqlItems = "SELECT * FROM npci_bank_list";
    $items = mysqli_query($conn, $sqlItems) or trigger_error("Query Failed: $sqlItems" .mysqli_error($conn));

    while ($row = mysqli_fetch_assoc($items)) {
        echo '<div data-value="'.$row['bank_code'].'">'.$row['bank_name'].'</div>';
    }
