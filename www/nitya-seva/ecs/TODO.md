# ECS Mandate ToDo

1. We have created the web page as per the mandate format prescribed by NPCI and specified the relevant indicators for mandatory and non-mandatory fields in the web page.

2. The web page displays the terms & conditions detailed in the mandate and acceptance of these terms and condition is a precondition for completing the mandate.  The customer acceptance check box is marked as 'No' by default and the customer has to agree to the terms and conditions by clicking on the relevant check box before proceeding ahead with further actions on the web page.

3. After the E-Mandate is registered and confirmation is received from Sponsor Bank we will, based on their reference number, update the UMRN ("Unique Mandate Reference Number") in our records.

4. We agree to be liable to the Sponsor Bank for any loss or damage on account of incorrect UMRN updation and its resulting consequences.

5. We declare that the URL and certificate shared for API E-mandate on-boarding is of our production environment.
