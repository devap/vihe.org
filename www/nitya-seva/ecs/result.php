<?php
//include '_0-EnvSetup.php';
/***
    TODO: parse results and post to database
    TODO: redirect to thanks or error page as appropriate
    TODO: Store results in Google Sheets
*/
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <title>VIHE ECS Mandate</title>
    <!--# include virtual="/payment/_head.inc" -->
    <style type="text/css">
        input[type="text"] {
            display: block;
        }
    </style>
</head>

<body>
    <div id="wrapper">
        <!--# include virtual="/_header.inc" -->
        <!-- start_page -->
        <div class="page">

            <div class="center-well-ecsmandate">

                <div class="page_title" align="center">
                    Vrindavan Institute for Higher Education<br />
                    <small>

                        ECS Mandate Application Form
                    </small>
                </div>

                <?php if (strcasecmp($_SERVER['REQUEST_METHOD'], 'POST') == 0) { ?>

                    <div class="container-fluid">

                        <form id="PostToMerchant" name="PostToMerchant" action="" method="POST">
                            <input type="hidden" ID="CheckSumVal" name="CheckSumVal" value="aeea600b85da7bd2e700387b365895fdb51bc6ca463095addbacdff52109b03c">
                            <input type="hidden" ID="MandateRespDoc" name="MandateRespDoc" value="{'Status':'Failed','MsgId':'29072019dev010','RefId':'0000002272','Errors':[{'Error_Code':'AP23','Error_Message':'Rejected as per customer confirmation'}],'Filler1':'Filler1','Filler2':'Filler2','Filler3':'Filler3','Filler4':'Filler4','Filler5':'Filler5','Filler6':'Filler6','Filler7':'Filler7','Filler8':'Filler8','Filler9':'Filler9','Filler10':'Filler10'}">
                        </form>


                        'Status':'Success',
                        Errorcode will be 000 and errormessage will be N/A in case of success.

                    </div>

                <?php } //end if POST
                ?>

            </div><!-- /.center-well-ecsmandate -->

            <!--# include virtual="/_footer.inc" -->

        </div><!-- / #page -->
    </div><!-- / #wrapper -->
    <!--# include virtual="/payment/_bottom.inc" -->
    <script src="/js/jquery-dateFormat.min.js"></script>
    <!--# include virtual="/nitya-seva/ecs/_js_formval.html" -->
</body>

</html>
