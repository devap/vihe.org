<?php

$insertSQL = sprintf(
    "INSERT INTO ecsmandate (msgid, debit_amount, debit_type, sequence_type, fullname, account_id, email, phone, start_date, end_date, debit_frequency, account_type, bank_code, bank_ifsc, customer_reference1, customer_reference2, ipaddress) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)",

    GetSQLValueString($MsgId, 'text'),
    GetSQLValueString($Customer_DebitAmount, 'double'),
    GetSQLValueString($Channel, 'text'),
    GetSQLValueString($Customer_SequenceType, 'text'),
    GetSQLValueString($Customer_Name, 'text'),
    GetSQLValueString($Customer_AccountNo, 'text'),
    GetSQLValueString($Customer_EmailId, 'text'),
    GetSQLValueString($Customer_Mobile, 'text'),
    GetSQLValueString($Customer_StartDate, 'date'),
    GetSQLValueString($Customer_ExpiryDate, 'date'),
    GetSQLValueString($Customer_DebitFrequency, 'text'),
    GetSQLValueString($Filler5, 'text'),
    GetSQLValueString($Filler6, 'text'),
    GetSQLValueString($Customer_InstructedMemberId, 'text'),
    GetSQLValueString($Customer_Reference1, 'text'),
    GetSQLValueString($Customer_Reference2, 'text'),
    GetSQLValueString($ipaddress, 'text'));
