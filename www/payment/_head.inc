<!-- Global site tag (gtag.js) - Google Analytics -->
<script async data-cookieyes="cookieyes-analytics" src="https://www.googletagmanager.com/gtag/js?id=UA-47076740-2"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-47076740-2');
</script>


<!-- Start cookieyes banner -->
<script id="cookieyes" type="text/javascript" src="https://cdn-cookieyes.com/client_data/965b06f93e761faa105251be.js"></script>
<!-- End cookieyes banner -->

    <meta name="keywords" content="Srila Prabhupada, institute, Vrindavan, education, online, education, degree, diploma, Vrindavan Institute for Higher Education, VIHE" />
    <meta name="description" content="The VIHE in Vrindavan was established to fulfill Srila Prabhupada's desire for an educational institute in Vrindavana." />
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />

    <title>VIHE Payment Gateway</title>

    <!-- Chromestyle -->
    <link rel="stylesheet" type="text/css" href="/chrometheme/chromestyle.css" />

    <!-- Bootstrap 4
    <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    -->

    <!-- Bootstrap 3 -->
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
    -->

    <link rel="stylesheet" type="text/css" href="/css/bootstrap-formhelpers.min.css" media="screen" />

    <link rel="stylesheet" type="text/css" href="/css/styles.css" media="screen" />
