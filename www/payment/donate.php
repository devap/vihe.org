<?php
session_start();
include '_0-EnvSetup.php';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>VIHE Donation Gateway</title>
<!--# include virtual="/payment/_head.inc" -->
</head>
<body>
    <div id="wrapper">

        <!--# include virtual="/_header.inc" -->

        <!-- start_page -->
        <div class="page">

        <div class="center_well_payment">

            <div class="page_title" align="center">
                Vrindavan Institute for Higher Education
            </div>

            <?php if(strcasecmp($_SERVER['REQUEST_METHOD'], 'GET') == 0) { ?>

            <div class="container-fluid">
                <form id="payment-form" class="needs-validation" action="" method="POST">
                    <input type="hidden" id="txn_type" name="udf2" value="donation" />

                    <?php include "_form_fields.php"; ?>

                    <div class="row bg-success" style="padding-top:10px;">
                        <div class="form-group form-group-sm col-xs-4" >
                            <div id="select_donation" class="pinfo">
                                <label control-label for="donation">Donation Category</label>
                                <select id="donation" name="donation" class="form-control">
                                    <option value="0">Select Project</option>
                                    <?php include '_get_donation_categories.php'; ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group form-group-sm col-xs-1">
                        </div>
                        <div class="form-group form-group-sm col-xs-4">
                            <label control-label for="course_amount">Amount</label>
                            <input type="text" class="form-control" id="amount" name="amount" placeholder="" required />
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group form-group-sm col-xs-4">
                            <div id="helpTxtCategory" class="help-block">Please select a Donation Category.</div>
                            <div id="helpTxtSpaceHolder" class="">&nbsp;</div>

                            <button id="submit_form" type="submit" class="btn btn-info">Submit</button>
                        </div>

                    </div>

                </form>
            </div>

            <?php } //end if GET

                // $udf1 contains the nationality
                if (isset($udf1)) {
                    if ($udf1 == 'IN' && $html) {
                        echo "Processing ...";
                        echo $html; //submit request to PayUBiz
                    } else {
                        redirect('bankpayment.php', 'success');
                    }
                }
            ?>

        </div>

        <!--# include virtual="/_footer.inc" -->

        </div><!-- / #page -->

    </div><!-- / #wrapper -->

    <!--# include virtual="/payment/_bottom.inc" -->
    <!--# include virtual="/payment/_js_formval.inc" -->

</body>
</html>
