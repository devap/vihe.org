<?php
include_once 'Functions.php';

$salt          = $_SERVER["PAYU_SALT"];
$merchant_key  = $_SERVER["PAYU_KEY"];
$txnid         = 'Txn' . rand(10000,99999999);

$PayU_URI_test = 'https://test.payu.in/_payment';
$PayU_URI_prod = 'https://secure.payu.in/_payment';
$action        = $PayU_URI_prod;

$html='';

if(strcasecmp($_SERVER['REQUEST_METHOD'], 'POST') == 0) {

    /***
    echo "<pre>_0-EnvSetup.php->POST: ";
    print_r($_POST);
    echo "</pre>";
    */

    include '_get_postvars.php';
    $ipaddress = getIPAddress();
    /***
        Account A = $merchant_key_testA (Donations from Indians)
        Account B = $merchant_key_testB (Donations from all other than Indians)
    */

    if (!isset($key)) {
        /***
        Account A $merchant_key_testA (Donations from Indians)
        Account B $merchant_key_testB (Donations from all others)
        if ($udf1 == 'IN') {
            $key = $merchant_key_testA;
        } else {
            $key = $merchant_key_testB;
        }
        */
        $key = $merchant_key;
    }

    $_SESSION['salt'] = $salt;

    //generate hash with mandatory parameters and udf5
    //$hashstr = "$key|$txnid|$amount|$productinfo|$firstname|$email|$udf1|$udf2|$udf3|$udf4|$udf5||||||$salt";
    $hashstr = "$key|$txnid|$amount|$productinfo|$firstname|$email|$udf1|$udf2|$udf3|$udf4|$udf5||||||$salt";
    $hash = hash('sha512', $hashstr);

    /****************
     *** DATABASE ***
     ****************/
    //save initial transaction to database
    include 'dbConfig.php';
    include '_sql_prep_insert.php';
    mysqli_select_db($conn, $database);
    $result = mysqli_query($conn, $insertSQL) or trigger_error("INSERT Failed: $insertSQL" .mysqli_error($conn), E_USER_ERROR);

    include '_set_form_html.php';
    $html = '<form action="'.$action.'" id="payment_form_submit" method="POST">
            '.$formfields.'
            <input type="hidden" name="surl" value="'.getSurl().'" />
            <input type="hidden" name="furl" value="'.getFurl().'" />
            <input type="hidden" name="key" value="'.$key.'" />
            <input type="hidden" name="hash" value="'.$hash.'" />
            <input type="hidden" name="txnid" value="'.$txnid.'" />
            </form>
            <script type="text/javascript"><!--
                document.getElementById("payment_form_submit").submit();
            //-->
            </script>';
}

/** This function is for dynamically generating callback url */
function getSurl()
{
    $protocol = ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
    $uri = str_replace('/index.php','/',$_SERVER['REQUEST_URI']);
    $uri = str_replace('/donate.php','/',$_SERVER['REQUEST_URI']);
    return $protocol . $_SERVER['HTTP_HOST'] . $uri . 'success.php';
}
function getFurl()
{
    $protocol = ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
    $uri = str_replace('/index.php','/',$_SERVER['REQUEST_URI']);
    $uri = str_replace('/donate.php','/',$_SERVER['REQUEST_URI']);
    return $protocol . $_SERVER['HTTP_HOST'] . $uri . 'error.php';
}
/***
function getCurl()
{
    $protocol = ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
    $uri = str_replace('/index.php','/',$_SERVER['REQUEST_URI']);
    $uri = str_replace('/donate.php','/',$_SERVER['REQUEST_URI']);
    return $protocol . $_SERVER['HTTP_HOST'] . $uri . 'cancel.php';
}
*/
