<?php

$updateSQL = sprintf("UPDATE `payubiz` SET `addedon`=%s, `status`=%s, `mihpayid`=%s, `mode`=%s, `bankcode`=%s, `error`=%s, `error_Message`=%s WHERE `txnid` = %s",

    GetSQLValueString($addedon, 'date'),
    GetSQLValueString($status, 'text'),
    GetSQLValueString($mihpayid, 'int'),
    GetSQLValueString($mode, 'text'),
    GetSQLValueString($bankcode, 'text'),
    GetSQLValueString($error, 'text'),
    GetSQLValueString($error_Message, 'text'),
    GetSQLValueString($txnid, 'text'));
