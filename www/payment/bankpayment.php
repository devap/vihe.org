<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>VIHE Payment Gateway: Thank You!</title>
<!--# include virtual="/payment/_head.inc" -->
</head>
<body>
    <div id="wrapper">

        <!--# include virtual="/_header.inc" -->
        <!--# include virtual="/_topnav.inc" -->

        <!-- start_page -->
        <div class="page">

        <div class="left_sidebar"></div>

        <div class="entry">
            <div class="page_title">
                Vrindavan Institute for Higher Education
            </div>

            <div>
                Thank you for your desire to support the VIHE. Please find below details of the Bank account where contributions can be sent. If you have any clarifications please contact Balaji Priya Devi Dasi on +91&nbsp;9821205738
                <br/><br/>
            </div>

            <div>

                <table class="table">
                    <caption>Central FCRA Bank Account Details</caption>
                  <thead>
                    <tr>
                      <th scope="col" colspan="4">Central FCRA Bank Account Details</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <th scope="row">Beneficiary or Account Name</th>
                      <td>International Society for Krishna Consciousness</td>
                    </tr>
                    <tr>
                      <th scope="row">Beneficiary Address</th>
                      <td>Hare Krishna Land, Juhu, Mumbai</td>
                    </tr>
                    <tr>
                      <th scope="row">Bank</th>
                      <td>State Bank of India</td>
                    </tr>
                    <tr>
                      <th scope="row">Branch</th>
                      <td>New Delhi Main Branch</td>
                    </tr>
                    <tr>
                      <th scope="row">Account Number</th>
                      <td>40132683851</td>
                    </tr>

                    <tr>
                      <th scope="row">Account Type</th>
                      <td>Account Number</td>
                    </tr>
                    <tr>
                      <th scope="row">IFSC Code</th>
                      <td>SBIN0000691</td>
                    </tr>
                    <tr>
                      <th scope="row">Branch Code for INR transfer</th>
                      <td>00691</td>
                    </tr>
                    <tr>
                      <th scope="row">Purpose Code for SWIFT Transfer</th>
                      <td>P1303 (Donations to religious and charitableinstitutions in India)</td>
                    </tr>
                    <tr>
                      <th scope="row">SWIFT Code for Foreign Currency Transfer</th>
                      <td>SBININBB104</td>
                    </tr>
                    <tr>
                      <th scope="row">Address of the branch</th>
                      <td>FCRA Cell, 4th Floor, State Bank of India, New Delhi Main Branch,11, Sansad Marg, New Delhi-110001.</td>
                    </tr>
                    <tr>
                      <th scope="row">FCRA Donor Letter</th>
                      <td>
                        <a href="FCRA-CentralIndia-Donor-Letter-V2.doc">Click here to Download</a>
                      </td>
                    </tr>

                  </tbody>
                </table>

            </div>


        <div>

            <ol>
                <li>
                    Use the above bank details to send a bank transfer.
                </li>
                <li>
                    Above FCRA donor letter is to be filled, printed, signed, scanned and emailed back to us along with a copy of your passport. This is to be done on the same day of the transfer.
                </li>
                <li>
                    It is to be noted here that the donor letter and passport is a prerequisite to claiming these funds at the bank. The bank holds the funds for a maximum of 3 days from receipt and if no donation letter and passport copy is received the funds are returned back.
                </li>
            </ol>

            <div>
                Please let us know once you initiate a transfer. The donation letter and passport copy can be sent by email to Balaji Priya Devi Dasi at balaji@vihe.org.
            </div>

        </div>

        </div><!-- /.entry -->
        <!--# include virtual="/_footer.inc" -->

        </div><!-- / #page -->

    </div><!-- / #wrapper -->

    <!--# include virtual="/payment/_bottom.inc" -->

</body>
</html>
