<?php

$message_txt_admin = <<<EOF
<html>
<head>
<title>VIHE Payment Gateway Transaction Notification</title>
</head>
<body>

VIHE.org Payment Gateway has received a $mail_subj: <br/><br/>

Transaction Id: $txnid <br/>
Name: $firstname $lastname <br/>
Email: $email <br/>
Amount: $amount <br/>
$product_type: $product_title <br/><br/>

Additional details:
<a href="https://docs.google.com/spreadsheets/d/1ByNGzUHowXGu_BkuzaxSbI7_vcBEaZkwjC-2OPNqWcs/edit#gid=0">VIHE Payment History</a>
</body>
</html>
EOF;
