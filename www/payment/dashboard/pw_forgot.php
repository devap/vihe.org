<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>VIHE Payment Gateway: Forgot Password</title>
<!--# include virtual="/payment/_head.inc" -->
</head>
<body>
    <div id="wrapper">

        <!--# include virtual="/_header.inc" -->

        <!-- start_page -->
        <div class="page">

        <div class="center_well_payment">

            <div class="page_title" align="center">
                Vrindavan Institute for Higher Education
            </div>

<?php

include('../dbConfig.php');

if(isset($_POST["email"]) && (!empty($_POST["email"]))) {

    $email = $_POST["email"];
    $email = filter_var($email, FILTER_SANITIZE_EMAIL);
    $email = filter_var($email, FILTER_VALIDATE_EMAIL);
    if (!$email) {
        $error .="<p>Invalid email address please type a valid email address!</p>";
    } else {
        $sqlUser = "SELECT * FROM `users` WHERE email='".$email."'";
        $results = mysqli_query($conn, $sqlUser);
        $row = mysqli_num_rows($results);
        if ($row == "") {
            $error .= "<p>No user is registered with this email address!</p>";
        }
    }

    if($error != "") {
        echo "<div class='error'>$error</div>
        <br/><a href='javascript:history.go(-1)'>Go Back</a>";
    } else {
        $expFormat = mktime(date("H"), date("i"), date("s"),
                            date("m"), date("d")+1, date("Y"));
        $expDate = date("Y-m-d H:i:s", $expFormat);

        $key = md5(2418 * 2 .$email);
        $addKey = substr(md5(uniqid(rand(),1)),3,10);
        $key = $key . $addKey;

        // Insert Temp Table
        $sqlInsert = "INSERT INTO `pw_reset_temp` (`email`, `key`, `expDate`) VALUES ('$email', '$key', '$expDate');";
        mysqli_query($conn, $sqlInsert) or trigger_error("INSERT Failed: $sqlInsert " , mysqli_error($conn));

        $output = '<p>Dear user,</p>';
        $output .='<p>Please click on the following link to reset your password.</p>';
        $output .= '<p>--------------------------------------------------</p>';
        $output.="<p><a href='https://vihe.org/payment/dashboard/pw_reset.php?
        key=$key&email=$email&action=reset' target='_blank'>Reset Password</a></p>";
        $output.='<p>----------------------------------------------------</p>';
        $output.='<p>Please be sure to copy the entire link into your browser.
        The link will expire after 1 day for security reason.</p>';
        $output.='<p>If you did not request this forgotten password email, no action is needed, your password will not be reset.</p>';
        $output.='<p>Thanks,</p>';

        $body = $output;
        $subject = "VIHE.org - Password Recovery";

        //MAIL SECTION START
        $email_to = $email;
        $fromserver = "noreply@vihe.org";
        // It is mandatory to set the content-type when sending HTML email
        $headers = "MIME-Version: 1.0\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8\r\n";

        // More headers. From is required, rest other headers are optional
        $headers .= "From: <$fromserver>\r\n";

        mail($email_to, $subject, $body, $headers);

        echo "<div class='error'>An email has been sent to you with instructions on how to reset password.</div>";

        /***
        require("PHPMailer/PHPMailerAutoload.php");
        $mail = new PHPMailer();
        $mail->IsSMTP();
        $mail->Host = "mail.vihe.org"; // Enter your host here
        $mail->SMTPAuth = true;
        $mail->Username = "noreply@vihe.org"; // Enter your email here
        $mail->Password = "password"; //Enter your password here
        $mail->Port = 25;
        $mail->IsHTML(true);
        $mail->From = "noreply@yourwebsite.com";
        $mail->FromName = "AllPHPTricks";
        $mail->Sender = $fromserver; // indicates ReturnPath header
        $mail->Subject = $subject;
        $mail->Body = $body;
        $mail->AddAddress($email_to);

        if(!$mail->Send()) {
            echo "Mailer Error: " . $mail->ErrorInfo;
        } else {
            echo "<div class='error'><p>An email has been sent to you with instructions on how to reset password.</p></div><br/><br/><br/>";
        }
        */

        //END MAIL SECTION

    }

} else { ?>

            <form name="reset" class="form-horizontal" method="post" action="">

                <div class="form-group">
                    <label for="email" class="col-sm-1 control-label">Email Address:</label>
                    <div class="col-sm-6">
                        <input id="email" type="email" name="email" class="form-control" placeholder="username@email.com" required value="" />
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-offset-1 col-sm-6">
                      <button type="submit" class="btn btn-primary">Reset Password</button>
                    </div>
                </div>

            </form>

<?php } ?>

        </div>

        <!--# include virtual="/_footer.inc" -->

        </div><!-- / #page -->

    </div><!-- / #wrapper -->

    <!--# include virtual="/payment/_bottom.inc" -->

</body>
</html>
