<?php
require('../dbConfig.php');
include('auth.php');
$status = "";
if(isset($_POST['new']) && $_POST['new'] == 1) {
    $code = $_REQUEST['code'];
    $price = $_REQUEST['price'];
    $title = $_REQUEST['title'];
    $sqlInsert = "INSERT INTO courses_retreats (`code`,`price`,`title`) VALUES
    ('$code','$price','$title')";
    mysqli_query($conn, $sqlInsert) or trigger_error("INSERT Failed: $sqlInsert". mysqli_error($conn));
    $status = "New Record Inserted Successfully.
    </br></br><a href='.'>View Inserted Record</a>";
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>VIHE Payment Gateway: Insert Course/Retreat</title>
<!--# include virtual="/payment/_head.inc" -->
</head>
<body>
    <div id="wrapper">

        <!--# include virtual="/_header.inc" -->

        <!-- start_page -->
        <div class="page">

        <div class="center_well_payment">

            <div class="page_title" align="center">
                Vrindavan Institute for Higher Education
            </div>

                <div class="" align="center">
                    <a href=".">View Records</a> |
                    <a href="logout.php">Logout</a></p>
                </div>

                <div class="container">

                <h3>Insert New Course or Retreat</h3><br/>

                    <form name="form" class="form-horizontal" method="post" action="">

                        <input type="hidden" name="new" value="1" />

                        <div class="form-group">
                            <label for="code" class="col-sm-1 control-label">Code</label>
                            <div class="col-sm-6">
                                <input type="text" id="code" name="code" class="form-control" placeholder="Code" required />
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="price" class="col-sm-1 control-label">Price</label>
                            <div class="col-sm-6">
                                <input type="text" name="price" class="form-control" placeholder="Price" required />
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="title" class="col-sm-1 control-label">Title</label>
                            <div class="col-sm-6">
                                <input type="text" name="title" class="form-control" placeholder="Title" required />
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-offset-1 col-sm-6">
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                        </div>

                    </form>
                    <p style="color:#f00;"><?php echo $status; ?></p>
                </div>


    </div>

    <!--# include virtual="/_footer.inc" -->

    </div><!-- / #page -->

</div><!-- / #wrapper -->

<!--# include virtual="/payment/_bottom.inc" -->

</body>
</html>
