<?php
require('../dbConfig.php');
include('auth.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>VIHE Payment Gateway: Data Dashboard</title>
<!--# include virtual="/payment/_head.inc" -->
</head>
<body><body>
    <div id="wrapper">

        <!--# include virtual="/_header.inc" -->

        <!-- start_page -->
        <div class="page">

        <div class="center_well_payment">

            <div class="page_title" align="center">
                Vrindavan Institute for Higher Education
            </div>

            <div class="" align="center">
                <a href="insert_course_retreat.php">Insert Course/Retreat</a>
                | <a href="insert_donation_category.php">Insert Donation Category</a>
                | <a href="logout.php">Logout</a>
            </div>

            <h4>Current Courses/Retreats</h4>
            <div align="center">

                <table width="60%" class="table table-striped table-hover">
                    <thead>
                        <tr>
                            <th><strong>Code</strong></th>
                            <th><strong>Price</strong></th>
                            <th><strong>Title</strong></th>
                            <th><strong></strong></th>
                            <th><strong></strong></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $sqlSelect = "SELECT * FROM courses_retreats ORDER BY id ASC;";
                        $result = mysqli_query($conn, $sqlSelect) or trigger_error("SELECT Failed: $sqlSelect");
                        while($row = mysqli_fetch_assoc($result)) { ?>
                            <tr>
                                <td width="35%" align="left"><?php echo $row["code"]; ?></td>
                                <td width="10%" align="center"><?php echo $row["price"]; ?></td>
                                <td width="35%" align="left"><?php echo $row["title"]; ?></td>
                                <td width="10%" align="center"><a href="edit_course_retreat.php?id=<?php echo $row["id"]; ?>">Edit</a></td>
                                <td width="10%" align="center"><a href="delete_course_retreat.php?id=<?php echo $row["id"]; ?>">Delete</a></td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>

            <h4>Current Donation Categories</h4>
            <div align="center">

                <table width="60%" class="table table-striped table-hover">
                    <thead>
                        <tr>
                            <th><strong>Code</strong></th>
                            <th><strong>Title</strong></th>
                            <th><strong></strong></th>
                            <th><strong></strong></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $sqlSelect = "SELECT * FROM donation_categories ORDER BY id ASC;";
                        $result = mysqli_query($conn, $sqlSelect) or trigger_error("SELECT Failed: $sqlSelect");
                        while($row = mysqli_fetch_assoc($result)) { ?>
                            <tr>
                                <td width="35%" align="left"><?php echo $row["code"]; ?></td>
                                <td width="35%" align="left"><?php echo $row["title"]; ?></td>
                                <td width="10%" align="center"><a href="edit_donation_category.php?id=<?php echo $row["id"]; ?>">Edit</a></td>
                                <td width="10%" align="center"><a href="delete_donation_category.php?id=<?php echo $row["id"]; ?>">Delete</a></td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>

        </div>

        <!--# include virtual="/_footer.inc" -->

        </div><!-- / #page -->

    </div><!-- / #wrapper -->

    <!--# include virtual="/payment/_bottom.inc" -->

</body>
</html>
