<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>VIHE Payment Gateway: Dashboard Login</title>
<!--# include virtual="/payment/_head.inc" -->
</head>
<body>
    <div id="wrapper">

        <!--# include virtual="/_header.inc" -->

        <!-- start_page -->
        <div class="page">

        <div class="center_well_payment">

            <div class="page_title" align="center">
                Vrindavan Institute for Higher Education
            </div>
<?php
require('../dbConfig.php');
session_start();
// If form submitted, insert values into the database.
if (isset($_POST['username'])) {
    // removes backslashes
    $username = stripslashes($_REQUEST['username']);
    //escapes special characters in a string
    $username = mysqli_real_escape_string($conn, $username);
    $password = stripslashes($_REQUEST['password']);
    $password = mysqli_real_escape_string($conn,$password);
    //Checking is user existing in the database or not
    $sqlSelect = "SELECT * FROM `users` WHERE username='$username' AND password='".md5($password)."'";
    $result = mysqli_query($conn, $sqlSelect) or trigger_error("SELECT Failed: $sqlSelect" . mysql_error());
    $rows = mysqli_num_rows($result);
    if($rows == 1) {
        $_SESSION['username'] = $username; // Redirect user to index.php
        header("Location: index.php");
    } else {
        echo "<div class='form'>
        <h3>Username/password is incorrect.</h3>
        <br/>Click here to <a href='login.php'>Login</a></div>";
    }
} else {
?>
<div class="form">
    <h4 style="p-b-5;">Log In</h4>

    <form name="login" class="form-horizontal" method="post" action="">

        <div class="form-group">
            <label for="username" class="col-sm-1 control-label">Username</label>
            <div class="col-sm-6">
                <input type="text" name="username" class="form-control-sm" placeholder="Username"required value="" />
            </div>
        </div>

        <div class="form-group">
            <label for="password" class="col-sm-1 control-label">Password</label>
            <div class="col-sm-6">
                <input type="password" name="password" class="form-control-sm" placeholder="Password"required value="" />
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-offset-1 col-sm-1">
              <button type="submit" class="btn btn-primary">Login</button>
            </div>

            <div class="col-sm-2">
                <br/><a href='pw_forgot.php'>Reset Password</a>
            </div>
        </div>



    </form>

    <!--
    <p>Not registered yet? <a href='registration.php'>Register Here</a></p>
    -->

</div>

<?php } ?>

    </div>

    <!--# include virtual="/_footer.inc" -->

    </div><!-- / #page -->

</div><!-- / #wrapper -->

<!--# include virtual="/payment/_bottom.inc" -->

</body>
</html>
