<?php
require('../dbConfig.php');
include('auth.php');

$id = $_REQUEST['id'];
$query = "SELECT * from courses_retreats where id='$id'";
$result = mysqli_query($conn, $query) or trigger_error("SELECT Failed: $sqlSelect " .mysqli_error($conn));
$row = mysqli_fetch_assoc($result);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>VIHE Payment Gateway: Edit Donation Categories</title>
<!--# include virtual="/payment/_head.inc" -->
</head>
<body>
    <div id="wrapper">

        <!--# include virtual="/_header.inc" -->

        <!-- start_page -->
        <div class="page">

        <div class="center_well_payment">

            <div class="page_title" align="center">
                Vrindavan Institute for Higher Education
            </div>

    <div class="" align="center">
        <p><a href=".">View Records</a>
        | <a href="insert_course_retreat.php">Insert Course/Retreat</a>
        | <a href="insert_donation_category.php">Insert Donation Category</a>
        | <a href="logout.php">Logout</a></p>
    </div>

    <div class="container">

        <h3>Update Course/Retreat</h3><br/>

    <?php
    $status = "";
    if(isset($_POST['new']) && $_POST['new']==1) {
        $id = $_REQUEST['id'];
        $code = $_REQUEST['code'];
        $title = $_REQUEST['title'];
        $sqlUpdate="UPDATE courses_retreats SET code = '$code', title='$title' WHERE id='$id'";
        mysqli_query($conn, $sqlUpdate) or trigger_error("UPDATE Failed: $sqlUpdate " .mysqli_error());
        $status = "Course Updated Successfully. </br></br>
        <a href='.'>View Updated Course</a>";
        echo '<p style="color:#f00;">'.$status.'</p>';
    } else {
    ?>

        <form name="form" class="form-horizontal" method="post" action="">

                <input type="hidden" name="new" value="1" />
                <input type="hidden" name="id" value="<?php echo $row['id'];?>" />

            <div class="form-group">
                <label for="code" class="col-sm-1 control-label">Code</label>
                <div class="col-sm-6">
                    <input type="text" name="code" class="form-control" placeholder="Code"required value="<?php echo $row['code'];?>" />
                </div>
            </div>

            <div class="form-group">
                <label for="title" class="col-sm-1 control-label">Title</label>
                <div class="col-sm-6">
                    <input type="text" name="title" class="form-control" placeholder="Title"required value="<?php echo $row['title'];?>" />
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-1 col-sm-6">
                  <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </div>

        </form>
    </div>
    <?php } ?>


    </div>

    <!--# include virtual="/_footer.inc" -->

    </div><!-- / #page -->

</div><!-- / #wrapper -->

<!--# include virtual="/payment/_bottom.inc" -->

</body>
</html>
