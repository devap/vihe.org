<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>VIHE Payment Gateway: Password Reset</title>
<!--# include virtual="/payment/_head.inc" -->
</head>
<body>
    <div id="wrapper">

        <!--# include virtual="/_header.inc" -->

        <!-- start_page -->
        <div class="page">

        <div class="center_well_payment">

            <div class="page_title" align="center">
                Vrindavan Institute for Higher Education
            </div>

<?php
include('../dbConfig.php');

if (isset($_GET["key"]) && isset($_GET["email"]) && isset($_GET["action"])
&& ($_GET["action"]=="reset") && !isset($_POST["action"])) {
  $key = $_GET["key"];
  $email = $_GET["email"];
  $curDate = date("Y-m-d H:i:s");
  $sqlSelect = "SELECT * FROM `pw_reset_temp` WHERE `key` = '$key' and `email` = '$email';";
  $query = mysqli_query($conn, $sqlSelect) or trigger_error("SELECT Failed: $sqlSelect");
  $row = mysqli_num_rows($query);

  if ($row == "") {
    $error .= '<h4 style="p-b-5">Invalid Link</h4>
    <p>The link is invalid/expired. Either you did not copy the correct link
    from the email, or you have already used the key in which case it is
    deactivated.</p>
    <p><a href="pw_forgot.php">Reset password</a></p>';

  } else {

    $row = mysqli_fetch_assoc($query);
    $expDate = $row['expDate'];
    if ($expDate >= $curDate) {
      ?>

              <form name="update" class="form-horizontal" method="post" action="">
                <input type="hidden" name="action" value="update" />
                <input type="hidden" name="email" value="<?php echo $email;?>"/>

                <div class="form-group">
                    <label for="pass1" class="col-sm-3 control-label">Enter New Password:</label>
                    <div class="col-sm-6">
                        <input id="pass1" type="password" name="pass1" class="form-control" maxlength="15" required value="" />
                    </div>
                </div>

                <div class="form-group">
                    <label for="pass2" class="col-sm-3 control-label">Re-Enter New Password:</label>
                    <div class="col-sm-6">
                        <input id="pass2" type="password" name="pass2" class="form-control"  maxlength="15" required value="" />
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-6">
                      <button type="submit" class="btn btn-primary">Reset Password</button>
                    </div>
                </div>

              </form>

      <?php
    } else {
      $error .= "<h2>Link Expired</h2>
      <p>The link is expired. You are trying to use the expired link which
      as valid only 24 hours (1 days after request).</p>";
    }
  }
  if($error != "") {
    echo "<div class='error'>$error</div><br/>";
  }
} // isset email key validate end

if(isset($_POST["email"]) && isset($_POST["action"]) &&
 ($_POST["action"] == "update")) {
  $error = "";
  $pass1 = mysqli_real_escape_string($conn, $_POST["pass1"]);
  $pass2 = mysqli_real_escape_string($conn, $_POST["pass2"]);
  $email = $_POST["email"];
  $curDate = date("Y-m-d H:i:s");
  if ($pass1 != $pass2) {
    $error.= "<p>Password do not match, both password should be same.<br /><br /></p>";
  }
  if($error != "") {
    echo "<div class='error'>$error</div><br/>";
  } else {

    $pass1 = md5($pass1);
    $sqlUpdate = "UPDATE `users` SET `password` = '$pass1', `trn_date` = '$curDate' WHERE `email` = '$email';";
    mysqli_query($conn, $sqlUpdate) or trigger_error("UPDATE Failed: $sqlUpdate");

    $sqlDelete = "DELETE FROM `pw_reset_temp` WHERE `email` = '$email';";
    mysqli_query($conn, $sqlDelete) or trigger_error("DELETE Failed: $sqlDelete");

echo '<div class="error"><p>Congratulations! Your password has been updated successfully.</p>
  <p><a href="login.php">Login</a></p>
  </div>';
  }
}
?>
    </div>

    <!--# include virtual="/_footer.inc" -->

    </div><!-- / #page -->

</div><!-- / #wrapper -->

<!--# include virtual="/payment/_bottom.inc" -->

</body>
</html>
