<script type="text/javascript">
    //php include '_get_prices.php';?>

    function chkFields() {

        if ($('#phone').val() == '' || $('#phone').val() < 10) {
            $('#phone').addClass('has-error');
            $('#helpTxtPhone').show();
            return false;
        } else {
            $('#phone').removeClass('has-error');
            $('#helpTxtPhone').hide();
        }

        if ($('#nationality').val() == '') {
            $('#nationality').addClass('has-error');
            $('#helpTxtNationality').show();
            return false;
        } else {
            $('#nationality').removeClass('has-error');
            $('#helpTxtNationality').hide();
        }

        if ($('#country').val() == '') {
            // console.log("$('#country').val(): ", $('#country').val());
            $('#country').addClass('has-error');
            $('#helpTxtCountry').show();
            return false;
        } else {
            $('#country').removeClass('has-error');
            $('#helpTxtCountry').hide();
        }

        if ($('#state').val() == '') {
            // console.log("$('#state').val(): ", $('#country').val());
            $('#state').addClass('has-error');
            $('#helpTxtState').show();
            return false;
        } else {
            $('#state').removeClass('has-error');
            $('#helpTxtState').hide();
        }

        if($('#productinfo').val() == 0) {
            $('.pinfo').addClass('has-error');
            $('#helpTxtCategory').show();
            $('#helpTxtSpaceHolder').hide();
            return false;
        } else {
            $('.pinfo').removeClass('has-error');
            $('#helpTxtCategory').hide();
            $('#helpTxtSpaceHolder').show();
        }

        var zval = $('#zipcode').val();
        $('#zipcode').val(zval.replace(/\D/g,''));

        return true;
    }

    $('input[id="phone"]').keyup(function(){
        var telval = $('input[id="phone"]').val();
        $('input[id="phone"]').val(telval.replace(/\D/g,''));
        if ($('input[id="phone"]').val().length >= 10) {
            $('#helpTxtPhone').hide();
        }
    });

    $('input[name="fee_or_donation"]').on("change", function(event) {
        if (event.target.value == "fee") {
           $('#select_course_retreat').show();
           $('#select_donation').hide();
        }
        if (event.target.value == "donation") {
           $('#select_donation').show();
           $('#select_course_retreat').hide();
        }
    });

    $('select#course_retreat').on("change", function(event) {
        var val = event.target.value;
        //do not auto populate amount at this time
        //$('input[id="amount"]').val(prices[val]);

        if (val != '0') {
            $('#helpTxtCategory').hide();
            $('#helpTxtSpaceHolder').show();
        }

        $('#productinfo').val(val);

    });

    $('select#donation').on("change", function(event) {
        var val = event.target.value;
        //do not auto populate amount at this time
        //$('input[id="amount"]').val(prices[val]);

        if (val != '0') {
            $('#helpTxtCategory').hide();
            $('#helpTxtSpaceHolder').show();
        }
        $('#productinfo').val(val);
    });

    var frm = $('form#payment-form');
    frm.submit(function(event) {
        event.preventDefault();

        if (chkFields()) {
            frm[0].submit();
        }

    });

    // $(function() {
    //
    //     $('.bfh-selectbox').on('change.bfhselectbox', function (event) {
    //         console.log(event)
    //         window.donor_nationality = $(this).val();
    //         console.log('donor_nationality = ' + donor_nationality);
    //         // if (nationality == 'IN') {
    //         //     console.log('Nationality IS India');
    //         // } else {
    //         //     console.log('Nationality is NOT India');
    //         // }
    //     });
    //
    // });

</script>
