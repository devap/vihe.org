<?php
    require_once('dbConfig.php');

    mysqli_select_db($conn, $database);
    $sqlItems = "SELECT * FROM courses_retreats";
    $items = mysqli_query($conn, $sqlItems) or trigger_error("Query Failed: $sqlItems" .mysqli_error($conn));

    while ($row = mysqli_fetch_assoc($items)) {
        echo '<option value="'.$row['code'].'">'.$row['title'].'</option>';
    }
