<?php

$insertSQL = sprintf("INSERT INTO payubiz (txnid, txn_type, amount, productinfo, nationality, firstname, lastname, zipcode, email, phone, address1, address2, city, state, country, ipaddress) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)",

    GetSQLValueString($txnid, 'text'),
    GetSQLValueString($udf2, 'text'),
    GetSQLValueString($amount, 'double'),
    GetSQLValueString($productinfo, 'text'),
    GetSQLValueString($udf1, 'text'),
    GetSQLValueString($firstname, 'text'),
    GetSQLValueString($lastname, 'text'),
    GetSQLValueString($zipcode, 'text'),
    GetSQLValueString($email, 'text'),
    GetSQLValueString($phone, 'text'),
    GetSQLValueString($address1, 'text'),
    GetSQLValueString($address2, 'text'),
    GetSQLValueString($city, 'text'),
    GetSQLValueString($state, 'text'),
    GetSQLValueString($country, 'text'),
    GetSQLValueString($ipaddress, 'text'));
