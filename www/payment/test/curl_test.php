<?php

include 'curl.php';
$api_url = 'https://script.google.com/macros/s/AKfycbxsTNyhMpEKsMq_fkpgg4Y-LJQ_yjTZwJcxE5OcDHRoTIrBhUQ/exec';

$form = [];
$form['date']           = 'date';
$form['addedon']        = 'addedon';
$form['mihpayid']       = 'mihpayid';
$form['status']         = 'status';
$form['mode']           = 'mode';
$form['bankcode']       = 'bankcode';
$form['bank_ref_num']   = 'bank_ref_num';
$form['txn_type']       = 'udf2';
$form['nationality']    = 'udf1';
$form['firstname']      = 'firstname';
$form['lastname']       = 'lastname';
$form['productinfo']    = 'productinfo';
$form['email']          = 'email';
$form['phone']          = 'phone';
$form['address1']       = 'address1';
$form['city']           = 'city';
$form['state']          = 'state';
$form['zipcode']        = 'zipcode';
$form['country']        = 'country';
$form['txnid']          = 'txnid';
$form['amount']         = 'amount';
$form['error']          = 'error';
$form['error_Message']  = 'error_Message';

//post data to Google Sheets
$get_data = callAPI('GET', $api_url, $form);
$response = json_decode($get_data, true);
$errors = $response['response']['errors'];
$data = $response['response']['data'][0];
