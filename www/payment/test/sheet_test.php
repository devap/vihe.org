<?php

	require __DIR__ . "/vendor/autoload.php";

    $dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
    $dotenv->load();

    $spreadsheetId = $_ENV['SPREADSHEET_ID'];
    $range = $_ENV['GOOGLE_SHEET_RANGE'];
    //$valueInputOption = "USER_ENTERED";
    $valueInputOption = "RAW";
    $insertDataOption = "INSERT_ROWS";

    /*** standalone version */
    $client = new Google_Client();
    $client->setAccessType('offline');
    //$client->setAccessToken(getenv('GOOGLE_SERVICE_KEY'));
    $client->setAccessToken('4f70f8914b8f25e2564a6ccd1cd80eb169e998f2');
    $client->setApplicationName('VIHE Payment Gateway');
    $client->setAuthConfig(__DIR__ . '/credentials.json');
    //$client->setScopes([Google_Service_Sheets::SPREADSHEETS]);
    $client->setScopes(['https://www.googleapis.com/auth/spreadsheets']);

    $service = new Google_Service_Sheets($client);

    /*** OO version */
    //require __DIR__ . "/GoogleSheetsClient.php";
    //require __DIR__ . "/UpdateGoogleSheets.php";

    //$googleSheets = new GoogleSheetsClient();
    //$payment = new Payment( $googleSheets );

    $form = [];
    $form['date']           = 'date';
    $form['addedon']        = 'addedon';
    $form['mihpayid']       = 'mihpayid';
    $form['status']         = 'status';
    $form['mode']           = 'mode';
    $form['bankcode']       = 'bankcode';
    $form['bank_ref_num']   = 'bank_ref_num';
    $form['txn_type']       = 'udf2';
    $form['nationality']    = 'udf1';
    $form['firstname']      = 'firstname';
    $form['lastname']       = 'lastname';
    $form['productinfo']    = 'productinfo';
    $form['email']          = 'email';
    $form['phone']          = 'phone';
    $form['address1']       = 'address1';
    $form['city']           = 'city';
    $form['state']          = 'state';
    $form['zipcode']        = 'zipcode';
    $form['country']        = 'country';
    $form['txnid']          = 'txnid';
    $form['amount']         = 'amount';
    $form['error']          = 'error';
    $form['error_Message']  = 'error_Message';

    $values[] = $form;
    //print_r($values);

    $sheetInfo = $service->spreadsheets->get($spreadsheetId)->getProperties();
    print("Google Sheet: " .$sheetInfo['title']. PHP_EOL);
    print("Range: " .$range. PHP_EOL);

    /***
    $data0 = new Google_Service_Sheets_ValueRange();
    $data0->setValues([
        'values' => $values
    ]);
    */

    $txnid          = '12345';
    $firstname      = 'firstname';
    $lastname       = 'lastname';
    $email          = 'email';
    $amount         = 'amount';
    $udf2           = 'udf2';
    $productinfo    = 'productinfo';

    include "_email_txt.php";
    $message = $message_txt;
    //send email to VIHE team
    $mailto = "VIHE Payment Admin <gateway@vihe.org>";
    //send copy of email to VIHE admin
    $mailcc = "VIHE System Admin <devaprastha@vihe.org>";
    $subject = "VIHE Payment Gateway [$udf2]";

    $headers = "MIME-Version: 1.0\r\n";
    $headers .= "Content-type:text/html;charset=UTF-8\r\n";
    $headers .= "From: <noreply@vihe.org>\r\n";
    $headers .= "Cc: $mailcc\r\n";

    mail ( $mailto , $subject , $message, $headers );

    $body = new Google_Service_Sheets_ValueRange([
        'values' => $form
    ]);
    $params = [
        'valueInputOption' => $valueInputOption
    ];
    //$insert = ['insertDataOption' => $insertDataOption ];


    /** OO version
    $response = $googleSheets->updateSheet( $spreadsheetId, $range, $values );

    if ( $response->updatedRows ) {
        echo "Congratulations, $response->updatedRows row(s) updated on Google Sheets";
        echo "\$response: $response";
    }
    */

    try {
        $response = $service->spreadsheets_values->append(
            $spreadsheetId,
            $range,
            $body,
            $params
        );

    } catch(Exception $e) {
        echo "Exception: " . $e->getMessage();
    }

    //print_r($response);
    /***
    if ($response->getUpdates()->getUpdatedCells()) {
        printf("%d cells appended.", $response->getUpdates()->getUpdatedCells());
    }

    if ( $response->updatedRows ) {
        echo "Congratulations, $response->updatedRows row(s) updated on Google Sheets";
    }
    */
