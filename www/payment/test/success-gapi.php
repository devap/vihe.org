<?php
session_start();

if(isset($_SESSION['salt'])) {
    $SALT = $_SESSION['salt'];
} else {
    $SALT = '';
}

$hash_match = false;

if(strcasecmp($_SERVER['REQUEST_METHOD'], 'POST') == 0) {

    print_r($_POST);

    require __DIR__ . "/vendor/autoload.php";

    $dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
    $dotenv->load();

    /*** standalone version */
    $client = new Google_Client();
    $client->setAccessType('offline');
    //$client->setAccessToken(getenv('GOOGLE_SERVICE_KEY'));
    $client->setAccessToken('4f70f8914b8f25e2564a6ccd1cd80eb169e998f2');
    $client->setApplicationName('VIHE Payment Gateway');
    $client->setAuthConfig(__DIR__ . '/credentials.json');
    //$client->setScopes([Google_Service_Sheets::SPREADSHEETS]);
    $client->setScopes(['https://www.googleapis.com/auth/spreadsheets']);

    $service = new Google_Service_Sheets($client);
    $spreadsheetId = $_ENV['SPREADSHEET_ID'];
    $range = $_ENV['GOOGLE_SHEET_RANGE'];

    /*** OO version
    require __DIR__ . "/GoogleSheetsClient.php";
    require __DIR__ . "/UpdateGoogleSheets.php";

    $googleSheets = new GoogleSheetsClient();
    $payment = new Payment( $googleSheets );
    */


    include '_get_postvars.php';

    //$hashstr = "$SALT|$status||||||$udf5|$udf4|$udf3|$udf2|$udf1|$email|$firstname|$productinfo|$amount|$txnid|$key";
    $hashstr = "$SALT|$status|||||||||$udf2|$udf1|$email|$firstname|$productinfo|$amount|$txnid|".$_POST['key'];
    $this_hash = hash('sha512', $hashstr);
    $that_hash = $_POST['hash'];
    if ($this_hash === $that_hash) {
        $hash_match = true;
    }

    include '_set_form_array.php';
    $form_values[] = $form;

} ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>VIHE Payment Gateway: Success</title>
<!--# include virtual="/payment/_head.inc" -->
</head>
<body>
    <div id="wrapper">

        <!--# include virtual="/_header.inc" -->
        <!--# include virtual="/_topnav.inc" -->

        <!-- start_page -->
        <div class="page">
        <div class="left_sidebar"></div>

            <div class="entry">
                <div class="page_title" align="center">
                    Vrindavan Institute for Higher Education
                </div>

                <?php if($hash_match) {

                    include  __DIR__ . '/dbConfig.php';
                    include  __DIR__ . '/_sql_prep.php';
                    mysqli_select_db($conn, $database);
                    $Result1 = mysqli_query($conn, $insertSQL) or trigger_error("INSERT Failed: $insertSQL");

                    echo '<h1 style="color:green;">Payment Successful!</h1>';
                    echo '<div>Thank you for your participation!</div>';

                    //submit data for Google Sheets...

                    /** standalone version */
                    $data = new Google_Service_Sheets_ValueRange([
                        'values' => $form_values
                    ]);
                    /**
                    $params = [
                        'valueInputOption' => 'RAW'
                    ];
                    */
                    $params = [
                        'valueInputOption' => 'USER_ENTERED'
                    ];
                    $insert = [
                        "insertDataOption" => "INSERT_ROWS"
                    ];

                    try {
                        $response = $service->spreadsheets_values->append(
                            $spreadsheetId,
                            $range,
                            $data,
                            $params,
                            $insert
                        );

                    } catch(Exception $e) {
                        echo $e->getMessage();
                    }

                    if ( $response->updatedRows ) {
                        echo "Congratulations, $response->updatedRows row(s) updated on Google Sheets";

                        //send email to VIHE team
                        $mailto = $_ENV('SUCCES_EMAIL');
                    } else {
                        //send email to VIHE admin
                        $mailto = $_ENV('FAIL_EMAIL');
                    }

                    $mailfrom = "noreply@vihe.org";

                    /** OO version
                        $result = $payment->updateGoogleSheets($form_values);
                        echo "\$result: $result";
                    */

                    } //end if($hash_match)
                    ?>

            </div><!-- /.entry -->
            <!--# include virtual="/_footer.inc" -->

        </div><!-- / #page -->
    </div><!-- / #wrapper -->

    <!--# include virtual="/payment/_bottom.inc" -->

</body>
</html>
