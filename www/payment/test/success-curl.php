<?php
session_start();

if(isset($_SESSION['salt'])) {
    $SALT = $_SESSION['salt'];
} else {
    $SALT = '';
}

$hash_match = false;
$html = '';

include 'curl.php';
$api_url = 'https://script.google.com/macros/s/AKfycbxsTNyhMpEKsMq_fkpgg4Y-LJQ_yjTZwJcxE5OcDHRoTIrBhUQ/exec';

if(strcasecmp($_SERVER['REQUEST_METHOD'], 'POST') == 0) {

    //print_r($_POST);
    include '_get_postvars.php';

    //$hashstr = "$SALT|$status||||||$udf5|$udf4|$udf3|$udf2|$udf1|$email|$firstname|$productinfo|$amount|$txnid|$key";
    $hashstr = "$SALT|$status|||||||||$udf2|$udf1|$email|$firstname|$productinfo|$amount|$txnid|".$_POST['key'];
    $this_hash = hash('sha512', $hashstr);
    $that_hash = $_POST['hash'];
    if ($this_hash === $that_hash) {
        $hash_match = true;

        //save transaction to database
        include 'dbConfig.php';
        include '_sql_prep.php';
        mysqli_select_db($conn, $database);
        $Result1 = mysqli_query($conn, $insertSQL) or trigger_error("Result1: $insertSQL");

        //email notification to admins
        include "_email_txt.php";
        $message = $message_txt;
        //send email to VIHE team
        $mailto = "VIHE Payment Admin <gateway@vihe.org>";
        //send copy of email to VIHE admin
        $mailcc = "VIHE System Admin <devaprastha@vihe.org>";
        $subject = "VIHE Payment Gateway [$udf2]";

        $headers = "MIME-Version: 1.0\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8\r\n";
        $headers .= "From: <noreply@vihe.org>\r\n";
        $headers .= "Cc: $mailcc\r\n";

        mail ($mailto , $subject , $message, $headers);

        //post data to Google Sheets
        include '_set_form_array.php';
        $get_data = callAPI('GET', $api_url, $form);
        $response = json_decode($get_data, true);
        $errors = $response['response']['errors'];
        $data = $response['response']['data'][0];

    }

} ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>VIHE Payment Gateway: Success</title>
<!--# include virtual="/payment/_head.inc" -->
</head>
<body>
    <div id="wrapper">

        <!--# include virtual="/_header.inc" -->
        <!--# include virtual="/_topnav.inc" -->

        <!-- start_page -->
        <div class="page">

        <div class="left_sidebar"></div>

        <div class="entry">
            <div class="page_title" align="center">
                Vrindavan Institute for Higher Education
            </div>

            <?php
                if($hash_match) {
                    echo '<h1 style="color:green;">Payment Successful!</h1>';
                    echo '<div>Thank you for your support and participation!</div>';
                }
            ?>

        </div><!-- /.entry -->
        <!--# include virtual="/_footer.inc" -->

        </div><!-- / #page -->

    </div><!-- / #wrapper -->

    <!--# include virtual="/payment/_bottom.inc" -->

</body>
</html>
