<?php

require __DIR__ . '/vendor/autoload.php';

class GoogleSheetsClient
{
  protected $service;

  /**
  * GoogleSheetsClient constructor
  */
  public function __construct()
  {
    $apiKey = $_ENV['GOOGLE_API_KEY'];

    $client = new Google_Client();
    $client->setAccessType( 'offline' );
    $client->setApplicationName('VIHE Payment Gateway');
    $client->setScopes( [ 'https://www.googleapis.com/auth/spreadsheets' ] );

    $client->setDeveloperKey( $apiKey );
    $client->setSubject($_ENV['GOOGLE_SERVICE_ACCOUNT_NAME']);

    $this->service = new Google_Service_Sheets( $client );
  }

  /**
  * Creates an event
  *
  * @param array $eventDetails event details e.g summary, start, end, attendees, e.t.c
  *
  * @return array $user of a user
  */
  public function updateSheet( $spreadsheetId, $range, $values )
  {

    $sheetInfo = $this->service->spreadsheets->get($spreadsheetId)->getProperties();
    print("Google Sheet: " .$sheetInfo['title']. PHP_EOL);
    print("Range: " .$range. PHP_EOL);

    $requestBody = new Google_Service_Sheets_ValueRange( [
      'values' => $values
    ] );

    $params = [
      'valueInputOption' => 'USER_ENTERED'
    ];

    return $this->service->spreadsheets_values->update( $spreadsheetId, $range, $requestBody, $params );
  }
}
