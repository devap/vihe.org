<?php

/***
Request Hash
------------

For hash calculation, you need to generate a string using certain parameters
and apply the sha512 algorithm on this string. Please note that you have to
use pipe (|) character as delimeter.
The parameter order is mentioned below:

sha512(key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5||||||SALT)

Description of each parameter available on html page as well as in PDF.

Case 1: If all the udf parameters (udf1-udf5) are posted by the merchant. Then,
hash=sha512(key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5||||||SALT)

Case 2: If only some of the udf parameters are posted and others are not. For example, if udf2 and udf4 are posted and udf1, udf3, udf5 are not. Then,
hash=sha512(key|txnid|amount|productinfo|firstname|email||udf2||udf4|||||||SALT)

Case 3: If NONE of the udf parameters (udf1-udf5) are posted. Then,
hash=sha512(key|txnid|amount|productinfo|firstname|email|||||||||||SALT)

In present kit and available PayU plugins UDF5 is used. So the order is -
hash=sha512(key|txnid|amount|productinfo|firstname|email|||||udf5||||||SALT)

?>
