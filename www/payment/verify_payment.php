<?php
/***

For PayU Test Server:
POST URL: https://test.payu.in/_payment For PayU Production (LIVE) Server:
POST URL: https://secure.payu.in/_payment

Test Card Details
VISA CREDIT CARD
Card No:    4012 0010 3714 1112
Exp:        05/2021
CVV:        123
OTP :       123456
UAT TID:    9022137

For Failed Transaction testing please enter incorrect OTP & use rest all details same as mentioned above.
This is your UAT TID :9022137

Please find the below parameters.
PAYU
UDF1:   Product Info
UDF2:   Email ID
UDF3:   Phone
UDF4:   Address
UDF5:   Txn ID
*/

$merchant_name      = 'ISKCON VIHE Courses';
$merchant_acct      = '09427620000066';
$merchant_domain    = 'vihe.org';
$setup_type         = 'Hosted';
$key                = '7rnFly';
$salt               = 'pjVQAWpA';

$command = 'verify_payment';
$var1 = 'NPMM87334121'; //txnid

$hash_str = "$key|$command|$var1|$salt";
$hash = strtolower(hash('sha512', $hash_str));

$r = array('key' => $key,
           'hash' =>$hash,
           'var1' => $var1,
           'command' => $command
);

$qs = http_build_query($r);
$wsUrl = "https://test.payu.in/merchant/postservice.php?form=1";
//$wsUrl = "https://info.payu.in/merchant/postservice?form=1";

   	$c = curl_init();
    curl_setopt($c, CURLOPT_URL, $wsUrl);
    curl_setopt($c, CURLOPT_POST, 1);
    curl_setopt($c, CURLOPT_POSTFIELDS, $qs);
    curl_setopt($c, CURLOPT_CONNECTTIMEOUT, 30);
    curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($c, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($c, CURLOPT_SSL_VERIFYPEER, 0);
    $o = curl_exec($c);
    if (curl_errno($c)) {
      $sad = curl_error($c);
      throw new Exception($sad);
    }
    curl_close($c);

    $valueSerialized = @unserialize($o);
    if($o === 'b:0;' || $valueSerialized !== false) {
      print_r($valueSerialized);
    }
    print_r($o);
?>
