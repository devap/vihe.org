<?php

$message_txt_payer = <<<EOF
<html>
<head>
<title>VIHE Payment Gateway Transaction Notification</title>
</head>
<body>

Dear $firstname,<br/><br/>

Thank you for your VIHE.org $mail_subj. <br/><br/>

If you have any questions or need assistance, please contact us at:<br/>

<a href="mailto:$email_addy">$email_addy</a><br/><br/>

<b>Details:</b><br/>

Transaction Id: $txnid <br/>
Name: $firstname $lastname <br/>
Amount: $amount <br/>
$product_type: $product_title <br/><br/>

</body>
</html>
EOF;
