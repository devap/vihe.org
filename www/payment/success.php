<?php
include_once 'dbConfig.php';
include_once 'Functions.php';
session_start();
$verified = false;

if(isset($_SESSION['salt'])) {
    $salt = $_SESSION['salt'];
} else {
    $salt = '';
}

$api_url = 'https://script.google.com/macros/s/AKfycbxsTNyhMpEKsMq_fkpgg4Y-LJQ_yjTZwJcxE5OcDHRoTIrBhUQ/exec';

function verify_payment($key, $salt, $txnid, $status) {
    global $conn, $database;
    $happy_path = false;
    //$wsTest = "https://test.payu.in/merchant/postservice.php?form=2";
    $wsLive = "https://info.payu.in/merchant/postservice?form=2";
    $wsUrl = $wsLive;
    $command = 'verify_payment';
    $hash_str = "$key|$command|$txnid|$salt";
    $hash = strtolower(hash('sha512', $hash_str));

    $r = array('key' => $key,
               'hash' =>$hash,
               'var1' => $txnid,
               'command' => $command
    );

    $qs = http_build_query($r);

    try {
        $txn_data = curlAPI('POST', $wsUrl, $qs);
        if($txn_data != false)
        {
            $paymentAry = $txn_data['transaction_details'][$txnid];
            if($paymentAry['status'] == $status)
            {
                //pull values from database and compare to $o
                $selectSQL = sprintf("SELECT `txnid`, `amount`, `productinfo` FROM `payubiz` WHERE `txnid`=%s",
                   GetSQLValueString($txnid, 'text'));

                mysqli_select_db($conn, $database);
                $res = mysqli_query($conn, $selectSQL) or trigger_error("SELECT Failed: $selectSQL ". mysqli_error($conn), E_USER_ERROR);
                $row = $res->fetch_assoc();

                if ($row['txnid'] === $paymentAry['txnid'] && $row['amount'] === $paymentAry['transaction_amount'] && $row['productinfo'] === $paymentAry['productinfo'])
                {
                    $happy_path = true;
                }
            }
        }
        return $happy_path;

    } catch (Exception $e) {
        trigger_error("Exception: $e", E_USER_ERROR);
        return false;
    }
}

if(strcasecmp($_SERVER['REQUEST_METHOD'], 'POST') == 0) {

    if(isset($_SESSION['key'])) {
        $key = $_SESSION['key'];
    } else {
        $key = $_POST['key'];
    }

    include '_get_postvars.php';

    //$hashstr = "$salt|$status||||||$udf5|$udf4|$udf3|$udf2|$udf1|$email|$firstname|$productinfo|$amount|$txnid|$key";
    $hashstr = "$salt|$status||||||$udf5|$udf4|$udf3|$udf2|$udf1|$email|$firstname|$productinfo|$amount|$txnid|$key";

    $this_hash = hash('sha512', $hashstr);
    $that_hash = $_POST['hash'];
    if ($this_hash === $that_hash) {
        $verified = verify_payment($key, $salt, $txnid, $status);
        if ($verified) {
            /************
             * DATABASE *
             ************/

            //save final transaction details to database
            include '_sql_prep_update.php';
            mysqli_select_db($conn, $database);
            $res = mysqli_query($conn, $updateSQL) or trigger_error("UPDATE Failed: $updateSQL" .mysqli_error($conn), E_USER_ERROR);

            /**********
             * EMAILS
             *********/
            if ($udf2 == 'donation') {
                $product_type = "Donation";
                $mail_subj = $product_type;
                $email_addy = "donate@vihe.org";
                $mailfrom = "VIHE Donations <$email_addy>";
                $product_table = "donation_categories";
            } else {
                $product_table = "courses_retreats";
                if (preg_match('/Retreat/', $productinfo)) {
                    $product_type = "Retreat";
                    $mail_subj = "$product_type Registration";
                    $email_addy = "retreats@vihe.org";
                    $mailfrom = "VIHE Retreat Registrations <$email_addy>";
                } else {
                    $product_type = "Course";
                    $mail_subj = "$product_type Registration";
                    $email_addy = "courses@vihe.org";
                    $mailfrom = "VIHE Course Registrations <$email_addy>";
                }
            }

            $subject = "VIHE: $mail_subj";

            //get Product title for emails (node code)
            $selectSQL = sprintf("SELECT `title` FROM %s WHERE `code`=%s",
               $product_table,
               GetSQLValueString($productinfo, 'text'));

            mysqli_select_db($conn, $database);
            $res = mysqli_query($conn, $selectSQL) or trigger_error("SELECT Failed: $selectSQL ". mysqli_error($conn), E_USER_ERROR);
            $row = $res->fetch_assoc();
            $product_title = $row['title'];

            //send email to VIHE team
            $mailto = "VIHE Payment Admin <gateway@vihe.org>";
            $mailbcc = "VIHE System Admin <devaprastha@vihe.org>"; //while testing

            include '_email_txt_admin.php';
            $message = $message_txt_admin;

            $headers = "MIME-Version: 1.0\r\n";
            $headers .= "Content-type:text/html;charset=UTF-8\r\n";
            $headers .= "From: <noreply@vihe.org>\r\n";
            $headers .= "Bcc: $mailbcc\r\n";

            mail ( $mailto , $subject , $message, $headers);

            //email confirmation to payer/donor
            $mailto = $email;
            include '_email_txt_payer.php';
            $message = $message_txt_payer;

            $headers = "MIME-Version: 1.0\r\n";
            $headers .= "Content-type:text/html;charset=UTF-8\r\n";
            $headers .= "From: $mailfrom\r\n";
            $headers .= "Bcc: $mailbcc\r\n";

            mail ( $mailto , $subject , $message, $headers );

            /*********************
             *** GOOGLE SHEETS ***
             *********************/
            //get Product title for emails (node code)
            $selectSQL = sprintf("SELECT ipaddress FROM `payubiz` WHERE `txnid`=%s",
               GetSQLValueString($txnid, 'text'));

            mysqli_select_db($conn, $database);
            $res = mysqli_query($conn, $selectSQL) or trigger_error("SELECT Failed: $selectSQL ". mysqli_error($conn), E_USER_ERROR);
            $row = $res->fetch_assoc();
            $ipaddress = $row['ipaddress'];

            include '_set_form_array.php';
            $get_data = curlAPI('GET', $api_url, $form);
            //$response = json_decode($get_data, true);
            //$errors = $response['response']['errors'];
            //$data = $response['response']['data'][0];

            //We're done!
            redirect("thanks.php", "success");

        } else {
            //payment failed verification
            $status         = 'failed';
            $error          = 'INVALID_PAYMENT';
            $error_Message  = 'payment failed verification';

            $updateSQL = sprintf("UPDATE `payubiz` SET `status`=%s, `error`=%s, `error_Message`=%s WHERE `txnid`=%s",
                GetSQLValueString($status, 'text'),
                GetSQLValueString($error, 'text'),
                GetSQLValueString($error_Message, 'text'),
                GetSQLValueString($txnid, 'text'));
            mysqli_select_db($conn, $database);
            $result = mysqli_query($conn, $updateSQL) or trigger_error("UPDATE Failed: $updateSQL ". mysqli_error($conn), E_USER_ERROR);

            //echo $error;
            redirect("error.php", "$error: $error_Message");
            exit;
        }

    } else {
        //hash mismatch
        $status         = 'failed';
        $error          = 'HASH_MISMATCH';
        $error_Message  = 'no hash match';

        $updateSQL = sprintf("UPDATE `payubiz` SET `status`=%s, `error`=%s, `error_Message`=%s WHERE `txnid`=%s",
            GetSQLValueString($status, 'text'),
            GetSQLValueString($error, 'text'),
            GetSQLValueString($error_Message, 'text'),
            GetSQLValueString($txnid, 'text'));
        mysqli_select_db($conn, $database);
        $result = mysqli_query($conn, $updateSQL) or trigger_error("UPDATE Failed: $updateSQL ". mysqli_error($conn), E_USER_ERROR);

        //echo $error;
        redirect("error.php", "$error: $error_Message");
        exit;
    }

    //end if POST
} ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>VIHE Payment Gateway: Status</title>
<!--# include virtual="/payment/_head.inc" -->
</head>
<body>
    <div id="wrapper">

        <!--# include virtual="/_header.inc" -->
        <!--# include virtual="/_topnav.inc" -->

        <!-- start_page -->
        <div class="page">

        <div class="left_sidebar"></div>

        <div class="entry">
            <div class="page_title" align="center">
                Vrindavan Institute for Higher Education
            </div>

                <h1>Status</h1>
                <div><p>
                    No Pending Transactions.
                </p></div>

            </div>


        </div><!-- /.entry -->
        <!--# include virtual="/_footer.inc" -->

        </div><!-- / #page -->

    </div><!-- / #wrapper -->

    <!--# include virtual="/payment/_bottom.inc" -->

</body>
</html>
