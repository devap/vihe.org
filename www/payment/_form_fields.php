    <input type="hidden" id="txnid" name="txnid" value="<?php echo $txnid; ?>" />
    <input type="hidden" id="pg" name="pg" value="cc" />
    <input type="hidden" id="productinfo" name="productinfo" value="" />

    <div class="section-person bg-info">
        <div class="row row-name">
            <div class="form-group form-group-sm col-xs-5">
                <label control-label for="firstname">First Name</label>
                <input type="text" class="form-control" id="firstname" name="firstname" placeholder="" required />
            </div>
            <div class="form-group form-group-sm col-xs-5">
                <label control-label for="lastname">Last Name</label>
                <input type="text" class="form-control" id="lastname" name="lastname" placeholder="" required />
            </div>
        </div>

        <div class="row row-email">
            <div class="form-group form-group-sm col-xs-5">
                <label control-label for="email">Email address</label>
                <input type="email" class="form-control" id="email" name="email" aria-describedby="emailHelp" maxlength="50" required />
            </div>
            <div class="form-group form-group-sm col-xs-5">
                <label control-label for="phone">Phone</label><br/>
                <!--
                <select id="countries_phone" class="form-control bfh-countries" data-country="IN"></select>
                -->
                <input type="tel" id="phone" name="phone" class="form-control" maxlength="50" required />
                <br/>
                <small id="emailHelp" class="form-text text-muted">
                    (must contain chars 0 to 9 only).
                </small>
                <div id="helpTxtPhone" class="help-block">Please enter Phone #</div>
            </div>

        </div>

        <div class="row row-nationality">
            <div class="form-group form-group-sm col-xs-4">
                <label control-label for="nationality">Nationality</label>
                <div id="nationality" class="bfh-selectbox bfh-countries" data-name="nationality" data-country="" data-flags="true" required >
                </div>
                <div id="helpTxtNationality" class="help-block">Please select your Nationality</div>
            </div>
        </div>
    </div><!-- /.section-person -->
    <div class="section-address">
        <div class="row row-address">
            <div class="form-group form-group-sm col-xs-10" style="padding-top:10px;">
                <label control-label for="address1">Address1</label>
                <input type="text" class="form-control" id="address1" name="address1" required />
            </div>
        </div>

        <div class="row row-country">
            <div class="form-group form-group-sm col-xs-4">
                <label control-label for="country">Country</label>
                <div id="country" class="bfh-selectbox bfh-countries" data-name="country" data-flags="true" required >
                </div>
                <div id="helpTxtCountry" class="help-block">Please select your Country</div>
            </div>
            <div class="form-group form-group-sm col-xs-1">
            </div>
            <div class="form-group form-group-sm col-xs-4">
                <label control-label for="state">State</label>
                <div id="state" class="bfh-selectbox bfh-states" data-name="state" data-country="country" required >
                </div>
                <div id="helpTxtState" class="help-block">Please select your State</div>
            </div>
        </div>

        <div class="row row-city">
            <div class="form-group form-group-sm col-xs-4">
                <label control-label for="city">City</label>
                <input type="text" class="form-control" id="city" name="city" required />
            </div>

            <div class="form-group form-group-sm col-xs-1">
            </div>

            <div class="form-group form-group-sm col-xs-4">
                <label control-label for="zipcode">Zipcode/Pincode</label>
                <input type="text" class="form-control" id="zipcode" name="zipcode" required />
            </div>
        </div>
    </div><!-- /.section-address -->
