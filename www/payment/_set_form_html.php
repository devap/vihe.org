<?php

$formfields = <<<EOF
<input type="hidden" name="udf1" value="$udf1" />
<input type="hidden" name="udf2" value="$udf2" />
<input type="hidden" name="udf3" value="$udf3" />
<input type="hidden" name="udf4" value="$udf4" />
<input type="hidden" name="udf5" value="$udf5" />

<input type="hidden" name="date" value="$postdate" />
<input type="hidden" name="pg" value="$pg" />

<input type="hidden" name="amount" value="$amount" />
<input type="hidden" name="productinfo" value="$productinfo" />
<input type="hidden" name="firstname" value="$firstname" />
<input type="hidden" name="lastname" value="$lastname" />
<input type="hidden" name="email" value="$email" />
<input type="hidden" name="phone" value="$phone" />
<input type="hidden" name="address1" value="$address1" />
<input type="hidden" name="address2" value="$address2" />
<input type="hidden" name="city" value="$city" />
<input type="hidden" name="state" value="$state" />
<input type="hidden" name="zipcode" value="$zipcode" />
<input type="hidden" name="country" value="$country" />

<input type="hidden" name="error" value="$error" />
<input type="hidden" name="error_Message" value="$error_Message" />
EOF;
