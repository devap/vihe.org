<?php
session_start();

if(isset($_SESSION['salt'])) {
    $SALT = $_SESSION['salt'];
} else {
    $SALT = '';
}

$hash_match = false;
$html = '';

include 'curl.php';
$api_url = 'https://script.google.com/macros/s/AKfycbxsTNyhMpEKsMq_fkpgg4Y-LJQ_yjTZwJcxE5OcDHRoTIrBhUQ/exec';

if(strcasecmp($_SERVER['REQUEST_METHOD'], 'POST') == 0) {

    //print_r($_POST);

    include '_get_postvars.php';

    //$hashstr = "$SALT|$status||||||$udf5|$udf4|$udf3|$udf2|$udf1|$email|$firstname|$productinfo|$amount|$txnid|$key";
    $hashstr = "$SALT|$status|||||||||$udf2|$udf1|$email|$firstname|$productinfo|$amount|$txnid|".$_POST['key'];
    $this_hash = hash('sha512', $hashstr);
    $that_hash = $_POST['hash'];
    if ($this_hash === $that_hash) {
        $hash_match = true;
    }

    include '_set_formfields.php';
    $html = '<form action="" id="success_form" method="post">
            <input type="hidden" id="nationality" name="nationality" value="'.$udf1.'" />
            <input type="hidden" id="txn_type" name="txn_type" value="'.$udf2.'" />
               '.$formfields.'
            </form>';
    }
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>VIHE Payment Gateway: Success</title>
<!--# include virtual="/payment/_head.inc" -->
</head>
<body>
    <div id="wrapper">

        <!--# include virtual="/_header.inc" -->
        <!--# include virtual="/_topnav.inc" -->

        <!-- start_page -->
        <div class="page">

        <div class="left_sidebar">

        </div>

        <div class="entry">
            <div class="page_title" align="center">
                Vrindavan Institute for Higher Education
            </div>


            <?php
                if($hash_match) {

                    //save transaction to database
                    include 'dbConfig.php';
                    include '_sql_prep.php';
                    mysqli_select_db($conn, $database);
                    $Result1 = mysqli_query($conn, $insertSQL) or trigger_error("Result1: $insertSQL");

                    echo '<h1 style="color:green;">Payment Successful!</h1>';
                    echo '<div>Thank you for your support and participation!</div>';

                    //email notification to admins
                    include "_email_txt.php";
                    $message = $message_txt;
                    //send email to VIHE team
                    $mailto = "VIHE Payment Admin <gateway@vihe.org>";
                    //send copy of email to VIHE admin
                    $mailcc = "VIHE System Admin <devaprastha@vihe.org>";
                    $subject = "VIHE Payment Gateway [$udf2]";

                    $headers = "MIME-Version: 1.0\r\n";
                    $headers .= "Content-type:text/html;charset=UTF-8\r\n";
                    $headers .= "From: <noreply@vihe.org>\r\n";
                    $headers .= "Cc: $mailcc\r\n";

                    mail ( $mailto , $subject , $message, $headers );

                    //post data to Google Sheets
                    if($html) {
                        echo $html; //data for Google Sheets...
                    }
                }
            ?>

        </div><!-- /.entry -->
        <!--# include virtual="/_footer.inc" -->

        </div><!-- / #page -->

    </div><!-- / #wrapper -->

    <!--# include virtual="/payment/_bottom.inc" -->

    <?php if($hash_match) { ?>
    <script type="text/javascript">
        //https://medium.com/@dmccoy/how-to-submit-an-html-form-to-google-sheets-without-google-forms-b833952cc175
        var $form = $('form#success_form');
        var url = 'https://script.google.com/macros/s/AKfycbxsTNyhMpEKsMq_fkpgg4Y-LJQ_yjTZwJcxE5OcDHRoTIrBhUQ/exec';

        (function() {

            var jqxhr = $.ajax({
              url: url,
              method: "GET",
              dataType: "json",
              data: $form.serialize(),
              success: function(res) {
                  console.log('Data saved to Google Sheets'),
                  console.log('Submitting Form...')
              },
              error: function(res) {
                  console.log('Data NOT saved to Google Sheets' + res)
              }
            });

        })();

    </script>
    <?php
        //send email...

    } ?>
</body>
</html>
