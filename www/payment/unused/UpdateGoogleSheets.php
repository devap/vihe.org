<?php

class Payment
{
  private $googleSheets;

  public function __construct( GoogleSheetsClient $googleSheets)
  {
      $this->googleSheets = $googleSheets;
  }

  /**
   * Updates Google Sheets
   *
   * @param array $values - values of the fields that need
   * to be updated on Sheets
   *
   * @return void
   */
  public function updateGoogleSheets( $values )
  {
      $spreadsheetId = getenv( "SPREADSHEET_ID" );
      $range         = getenv( "GOOGLE_SHEET_RANGE" );

      $response = $this->googleSheets->updateSheet( $values, $range, $spreadsheetId );

      if ( $response->updatedRows ) {
          return "Congratulations, $response->updatedRows row(s) updated on Google Sheets";
      }
  }

}
