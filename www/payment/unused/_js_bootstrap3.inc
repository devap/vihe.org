<script type="text/javascript">
    //https://medium.com/@dmccoy/how-to-submit-an-html-form-to-google-sheets-without-google-forms-b833952cc175
    var $form = $('form#payment-form');
    var url = 'https://script.google.com/macros/s/AKfycbyIGSgCO4kYg1hwXOLTUnhmrE4Vi8js3RYGZhJKY_5aPMEICBk/exec';


    // JavaScript for disabling form submissions if there are invalid fields
    (function() {
      'use strict';
      window.addEventListener('load', function() {
        // Fetch all the forms we want to apply custom Bootstrap validation styles to
        var forms = document.getElementsByClassName('needs-validation');
        // Loop over them and prevent submission
        var validation = Array.prototype.filter.call(forms, function(form) {
          form.addEventListener('submit', function(event) {
            if (form.checkValidity() === false) {
              event.preventDefault();
              event.stopPropagation();
            } else {
                var jqxhr = $.ajax({
                  url: url,
                  method: "GET",
                  dataType: "json",
                  data: $form.serialize(),
                  success: function(res) {
                      console.log('Data saved to Google Sheets'),
                      console.log('Submitting Form...'),
                      //$form.submit()
                  },
                  error: function(res) {
                      console.log('Data NOT saved to Google Sheets' + res)
                  }
                });
            }
            form.classList.add('was-validated');
          }, false);
        });
      }, false);
    })();

</script>
