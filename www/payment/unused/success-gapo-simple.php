<?php
session_start();

require __DIR__ . '/vendor/autoload.php';

$client = new Google_Client();
$client->setAccessType('offline');
$client->setApplicationName('VIHE Payment Gateway');
$client->setScopes([Google_Service_Sheets::SPREADSHEETS]);

/*
 * The JSON auth file can be provided to the Google Client in two ways, one is as a string which is assumed to be the
 * path to the json file. This is a nice way to keep the creds out of the environment.
 *
 * The second option is as an array. For this example I'll pull the JSON from an environment variable, decode it, and
 * pass along.
 */
$jsonAuth = getenv('JSON_AUTH');
//$client->setAuthConfig(__DIR__ . '/credentials.json');
$client->setAuthConfig(json_decode($jsonAuth, true));

$sheets = new Google_Service_Sheets($client);
//$spreadsheetId = "1ByNGzUHowXGu_BkuzaxSbI7_vcBEaZkwjC-2OPNqWcs";
$spreadsheetId = getenv('SPREADSHEET_ID');
//$update_range = "Payments!Range”;

/*
 * With the Google_Client we can get a Google_Service_Sheets service object to interact with sheets
 */

if(isset($_SESSION['salt'])) {
    $SALT = $_SESSION['salt'];
} else {
    $SALT = '';
}

$hash_match = false;
$html = '';

if(strcasecmp($_SERVER['REQUEST_METHOD'], 'POST') == 0) {

    //print_r($_POST);
    /***
    $_POST:
    [mihpayid] => 403993715521499506 [mode] => CC [status] => success [unmappedstatus] => captured [key] => 7rnFly [txnid] => Txn3958956 [amount] => 21.00 [cardCategory] => domestic [discount] => 0.00 [net_amount_debit] => 21 [addedon] => 2020-08-24 11:43:43 [productinfo] => Course 102 [firstname] => Devaprastha [lastname] => Dasa [address1] => [address2] => [city] => [state] => [country] => IN [zipcode] => [email] => devaprastha@gmail.com [phone] => 2404748022 [udf1] => Course 102 [udf2] => devaprastha@gmail.com [udf3] => 2404748022 [udf4] => [udf5] => Txn3958956 [udf6] => [udf7] => [udf8] => [udf9] => [udf10] => [hash] => 6e86271996e16976f158ef0a295940bca1745c8024fa99c180a055da8037a168a24164cea0d755dcba7e6e54e6467fadfaddf503602679592275142511e40ae4 [field1] => 175440191909 [field2] => 961791 [field3] => 549077929326346 [field4] => TlRzaTEwRUZSUDZzNklKNXh5OFc= [field5] => 05 [field6] => [field7] => AUTHPOSITIVE [field8] => [field9] => [payment_source] => payu [PG_TYPE] => HDFCPG [bank_ref_num] => 549077929326346 [bankcode] => CC [error] => E000 [error_Message] => No Error [name_on_card] => Deva [cardnum] => 401200XXXXXX1112 [cardhash] => This field is no longer supported in postback params.
    */

    include '_get_postvars.php';

    $hash_match = false;
    //$hashstr = "$SALT|$status||||||$udf5|$udf4|$udf3|$udf2|$udf1|$email|$firstname|$productinfo|$amount|$txnid|$key";
    $hashstr = "$SALT|$status|||||||||||$email|$firstname|$productinfo|$amount|$txnid|".$_POST['key'];
    $this_hash = hash('sha512', $hashstr);
    $that_hash = $_POST['hash'];
    if ($this_hash === $that_hash) {
        $hash_match = true;
    }

    include '_set_formfields.php';
    $html = '<form action="" id="success_form" method="post">
               '.$formfields.'
            </form>';
    }
?>
<!--# include virtual="/payment/_top.inc" -->
<body>
    <div id="wrapper">

        <!--# include virtual="/_header.inc" -->
        <!--# include virtual="/_topnav.inc" -->

        <!-- start_page -->
        <div class="page">

        <div class="left_sidebar">

        </div>

        <div class="entry">
            <div class="page_title" align="center">
                Vrindavan Institute for Higher Education
            </div>


            <?php
                if($hash_match) {

                    include 'dbConfig.php';
                    include '_sql_prep.php';
                    mysqli_select_db($conn, $database);
                    $Result1 = mysqli_query($conn, $insertSQL) or trigger_error("Result1: $insertSQL");

                    echo '<div style="color:green;">Payment Successful!</div>';
                    echo '<div>Thank you for your participation!</div>';

                }
            ?>

        </div><!-- /.entry -->
        <!--# include virtual="/_footer.inc" -->

        </div><!-- / #page -->

    </div><!-- / #wrapper -->

    <!--# include virtual="/payment/_bottom.inc" -->

</body>
</html>
