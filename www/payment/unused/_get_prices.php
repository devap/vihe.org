<?php

require_once('dbConfig.php');

mysqli_select_db($conn, $database);

$sqlItems = "SELECT * FROM courses_retreats";
$items = mysqli_query($conn, $sqlItems) or trigger_error("Query Failed: $sqlItems" .mysqli_error($conn));

$prices["0"] = "";
while ($row = mysqli_fetch_assoc($items)) {
    $prices[$row["code"]] = $row['price'];
}

    $js_array = json_encode($prices);
    echo "var prices = ". $js_array . ";\n";
