<div class="center_well_payment container-fluid">
    <form id="payment-form" class="needs-validation" action="" method="POST">

        <input type="hidden" id="txnid" name="txnid" value="<?php echo $txnid ?>" />
        <input type="hidden" id="pg" name="pg" value="cc" />

        <div class="row">
            <div class="form-group form-group-sm col-xs-5">
                <label control-label for="firstname">First Name</label>
                <input type="text" class="form-control" id="firstname" name="firstname" placeholder="" required />
            </div>
            <div class="form-group form-group-sm col-xs-5">
                <label control-label for="lastname">Last Name</label>
                <input type="text" class="form-control" id="lastname" name="lastname" placeholder="" required />
            </div>
        </div>

        <div class="row">
            <div class="form-group form-group-sm col-xs-5">
                <label control-label for="udf2">Email address</label>
                <input type="email" class="form-control" id="email" name="email" aria-describedby="emailHelp" maxlength="50" required />
                <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone.</small>
                <div class="invalid-feedback">
                   Please enter your email address.
                </div>

            </div>
            <div class="form-group form-group-sm col-xs-6">
                <label control-label for="countries_phone">Phone</label><br/>
                <select class="form-control bfh-countries" style="display:inline; height:15px; width:40%; border-color:#555;" id="countries_phone" data-country="IN" data-flags="true"></select>

                <input style="display:inline; height:15px; width:40%;" type="tel" id="phone" name="phone" class="form-control bfh-phone" data-country="countries_phone" maxlength="50" />

                <!--input type="tel" class="form-control" id="phone" name="phone" maxlength="50" required /-->
                <div class="invalid-feedback">
                   Please enter your phone number <br/>(must contain chars 0 to 9 only).
                </div>
            </div>
        </div>

        <div class="row">
            <div class="form-group form-group-sm col-xs-10">
                <label control-label for="address1">Address1</label>
                <input type="text" class="form-control" id="address1" name="address1" />
            </div>
        </div>

        <div class="row">
            <div class="form-group form-group-sm col-xs-10">
                <label control-label for="address2">Address2</label>
                <input type="text" class="form-control" id="address2" name="address2" />
            </div>
        </div>

        <div class="row">
            <div class="form-group form-group-sm col-xs-4">
                <label control-label for="countries">Country</label>
                <div id="countries" class="bfh-selectbox bfh-countries" data-name="country" data-country="IN" data-flags="true">
                </div>
            </div>
            <div class="form-group form-group-sm col-xs-1">
            </div>
            <div class="form-group form-group-sm col-xs-4">
                <label control-label for="state">State</label>
                <div id="state" class="bfh-selectbox bfh-states" data-name="state" data-country="countries">
                </div>
            </div>
        </div>

        <div class="row">

            <div class="form-group form-group-sm col-xs-4">
                <label control-label for="city">City</label>
                <input type="text" class="form-control" id="city" name="city" />
            </div>

            <div class="form-group form-group-sm col-xs-1">
            </div>

            <div class="form-group form-group-sm col-xs-4">
                <label control-label for="zipcode">Zipcode</label>
                <input type="text" class="form-control" id="zipcode" name="zipcode" />
            </div>

        </div>

        <div class="row">
            <div id="select_product" class="form-group form-group-sm col-xs-4" >
                <label control-label for="productinfo">Course/Retreat</label>
                <div id="productinfo" class="bfh-selectbox" data-name="productinfo" data-value="0" aria-describedby="helpTxtProduct">
                  <div data-value="0">Select Course or Retreat</div>
                  <div data-value="1">Option 1</div>
                  <div data-value="2">Option 2</div>
                  <div data-value="3">Option 3</div>
                  <div data-value="4">Option 4</div>
                  <div data-value="5">Option 5</div>
                  <div data-value="6">Option 6</div>
                  <div data-value="7">Option 7</div>
                  <div data-value="8">Option 8</div>
                  <div data-value="9">Option 9</div>
                  <div data-value="10">Option 10</div>
                  <div data-value="11">Option 11</div>
                  <div data-value="12">Option 12</div>
                  <div data-value="13">Option 13</div>
                  <div data-value="14">Option 14</div>
                  <div data-value="15">Option 15</div>
                </div>
                <span id="helpTxtProduct" class="help-block">Please select Course or Retreat.</span>

            </div>
            <div class="form-group form-group-sm col-xs-1">
            </div>
            <div class="form-group form-group-sm col-xs-4">
                <label control-label for="amount">Amount</label>
                <input type="text" class="form-control" id="amount" name="amount" placeholder="₹" required />
            </div>
        </div>

        <div class="row">
            <div class="col">
                <button id="submit_form" type="submit" class="btn btn-info">Submit</button>
            </div>
        </div>

    </form>
</div>
