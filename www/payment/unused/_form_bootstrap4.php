<form id="payment-form" class="needs-validation" action="<?php echo $action ?>" method="POST" novalidate>

    <?php $date = date_create(); ?>
    <input type="hidden" id="udf1" name="udf1" value="<?php echo date_format($date,DATE_W3C);?>" />
    <input type="hidden" id="txnid" name="txnid" value="<?php echo $txnid ?>" />
    <input type="hidden" id="udf5" name="udf5" value="<?php echo $txnid ?>" />

    <?php //optional fields ?>
    <div class="form-row">
        <div class="form-group col">
            <label for="firstname">First Name</label>
            <input type="text" class="form-control form-control-sm" id="firstname" name="firstname" placeholder="" required />
        </div>
        <div class="form-group col">
            <label for="lastname">Last Name</label>
            <input type="text" class="form-control form-control-sm" id="lastname" name="lastname" placeholder="" required />
        </div>
    </div>

    <div class="form-row">
        <div class="form-group col">
            <label for="udf2">Email address</label>
            <input type="email" class="form-control form-control-sm" id="email" name="email" aria-describedby="emailHelp" maxlength="50" required />
            <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone.</small>
            <div class="invalid-feedback">
               Please enter your email address.
            </div>
        </div>
        <div class="form-group col">
            <label for="phone">Phone</label>
            <input type="text" class="form-control form-control-sm" id="phone" name="phone" maxlength="50" required />
            <div class="invalid-feedback">
               Please enter your phone number (must contain chars 0 to 9 only).
            </div>
        </div>
    </div>

    <div class="form-row">
        <div class="col">

            <div class="form-group">
                <label for="address1">Address1</label>
                <input type="text" class="form-control form-control-sm" id="address1" name="address1" />
            </div>
        </div>
    </div>

    <div class="form-row">
        <div class="col">

            <div class="form-group">
                <label for="address2">Address2</label>
                <input type="text" class="form-control form-control-sm" id="address2" name="address2" />
            </div>

        </div>
    </div>


    <div class="form-row">
        <div class="col">

            <div class="form-group">
                <label for="city">City</label>
                <input type="text" class="form-control form-control-sm" id="city" name="city" />
            </div>

        </div>
        <div class="col">

            <div class="form-group">
                <label for="state">State</label>
                <input type="text" class="form-control form-control-sm" id="state" name="state" />
            </div>

        </div>
        <div class="col">

            <div class="form-group">
                <label for="zipcode">Zipcode</label>
                <input type="text" class="form-control form-control-sm" id="zipcode" name="zipcode" />
            </div>

        </div>
    </div>

    <div class="form-row">
        <div class="col">

            <div class="form-group">
                <label for="country">Country</label>
                <!-- <input type="text" class="form-control form-control-sm" id="Country" name="Country" />-->
                <div class="bfh-selectbox bfh-countries" data-country="IN" data-flags="true">
                </div>
            </div>
        </div>
        <div class="col">
        </div>
    </div>

    <div class="form-row">
        <div class="form-group col">
            <label for="productinfo">Product Info</label>
            <input type="text" class="form-control form-control-sm" id="productinfo" name="productinfo" aria-describedby="productHelp" placeholder="Course or Retreat ID" required />
            <div class="invalid-feedback">
               Please enter Course or Retreat ID
            </div>
        </div>
        <div class="form-group col">
            <label for="amount">Amount</label>
            <input type="text" class="form-control form-control-sm" id="amount" name="amount" aria-describedby="costHelp" placeholder="₹" required />
            <div class="invalid-feedback">
                Please enter cost of Course or Retreat
            </div>
        </div>
    </div>

    <div class="form-row">
        <div class="col">
            <div class="form-group">
                <label for="credit_card_number">Credit Card Number</label>
                <input type="text" class="form-control form-control-sm" id="credit_card_number" name="credit_card_number" required />
                <div class="invalid-feedback">
                    Please enter a valid credit card.
                </div>
            </div>
        </div>
    </div>

    <div class="form-row">
        <div class="col">
            <div class="form-group">
                <label for="exp_date">Expiration Date</label>
                <input type="text" class="form-control form-control-sm" id="exp_date" name="exp_date" placeholder="mm/yyyy" required />
                <div class="invalid-feedback">
                    Please enter the expiration date.
                </div>
            </div>
        </div>
        <div class="col">
            <div class="form-group">
                <label for="cvv">CVV</label>
                <input type="text" class="form-control form-control-sm" id="cvv" name="cvv" required />
                <div class="invalid-feedback">
                    Please enter the cvv.
                </div>
            </div>
        </div>
    </div>





    <div class="form-row">
        <div class="col">
            <button id="payment_form" type="submit" class="btn btn-info">Submit</button>
        </div>
    </div>

</form>
