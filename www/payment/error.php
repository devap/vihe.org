<?php
include_once 'dbConfig.php';
include_once 'Functions.php';
session_start();

$api_url = 'https://script.google.com/macros/s/AKfycbxsTNyhMpEKsMq_fkpgg4Y-LJQ_yjTZwJcxE5OcDHRoTIrBhUQ/exec';

if(strcasecmp($_SERVER['REQUEST_METHOD'], 'POST') == 0) {

    /***
    echo "<pre>POST:<br/>"
    print_r($_POST);
    echo "</pre>";
    */

    include '_get_postvars.php';

    $selectSQL = sprintf("SELECT `amount`, `productinfo` FROM `payubiz` WHERE `txnid`=%s",
        GetSQLValueString($txnid, 'text'));

    mysqli_select_db($conn, $database);
    $res = mysqli_query($conn, $selectSQL) or trigger_error("SELECT Failed: $selectSQL ". mysqli_error($conn), E_USER_ERROR);
    $row = $res->fetch_assoc();

    if ($amount == $row['amount'] && $productinfo == $row['productinfo']) {

        $updateSQL = sprintf("UPDATE `payubiz` SET `addedon`=%s, `mihpayid`=%s, `status`=%s, `error`=%s, `error_Message`=%s WHERE `txnid`=%s",
                    GetSQLValueString($addedon, 'date'),
                    GetSQLValueString($mihpayid, 'text'),
                    GetSQLValueString($status, 'text'),
                    GetSQLValueString($error, 'text'),
                    GetSQLValueString($error_Message, 'text'),
                    GetSQLValueString($txnid, 'text'));

        mysqli_select_db($conn, $database);
        $result = mysqli_query($conn, $updateSQL) or trigger_error("UPDATE Failed: $updateSQL ". mysqli_error($conn), E_USER_ERROR);

    } else {
        $status         = 'failed';
        $error          = 'UNVERIFIED';
        $error_Message  = 'data not verified';

        $updateSQL = sprintf("UPDATE `payubiz` SET `status`=%s, `error`=%s, `error_Message`=%s WHERE `txnid`=%s",
            GetSQLValueString($status, 'text'),
            GetSQLValueString($error, 'text'),
            GetSQLValueString($error_Message, 'text'),
            GetSQLValueString($txnid, 'text'));
        mysqli_select_db($conn, $database);
        $result = mysqli_query($conn, $updateSQL) or trigger_error("UPDATE Failed: $updateSQL ". mysqli_error($conn), E_USER_ERROR);
    }

    /*********************
     *** GOOGLE SHEETS ***
     *********************/
    //post data to Google Sheets
    include '_set_form_array.php';
    $get_data = curlAPI('GET', $api_url, $form);

} //end if POST

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>VIHE Payment Gateway: Error</title>
<!--# include virtual="/payment/_head.inc" -->
</head>
<body>
    <div id="wrapper">

        <!--# include virtual="/_header.inc" -->
        <!--# include virtual="/_topnav.inc" -->

        <!-- start_page -->
        <div class="page">

        <div class="left_sidebar">

        </div>

        <div class="entry">
            <div class="page_title" align="center">
                Vrindavan Institute for Higher Education
            </div>

            <h1 style="color:red;">Transaction failed.</h1>

        </div>

        <!--# include virtual="/_footer.inc" -->

        </div><!-- / #page -->

    </div><!-- / #wrapper -->

    <!--# include virtual="/payment/_bottom.inc" -->

</body>
</html>
