<div class="left_nav_top_bg"> </div>
<div class="left_nav_bg">
<table>
  <tr><td><a href="index.html">Home</a></td></tr>
  <tr><td><a href="audio_lectures.html">Audio lectures</a></td></tr>
  <!--<tr><td><a href="/donate">Support Us</a></td></tr>-->
  <tr><td><a href="books.html">Books</a></td></tr>
  <tr><td><a href="news.html">Newsletter</a></td></tr>
  <tr><td><a href="asramas.html">Accommodation</a></td></tr>
  <tr><td><a href="prasadam.html">Prasadam</a></td></tr>
  <tr><td><a href="health.html">Health</a></td></tr>
  <tr><td><a href="testimonals.html">Testimonials</a></td></tr>
  <tr><td><a href="faq.html">FAQ</a></td></tr>
  <tr><td><a href="contact_us.html">Contact us</a></td></tr>
</table>
</div>


<div class="green_left_nav_top_bg"> </div>
<div class="green_left_nav_bg">
	<strong>Visit <a href="/audio_lectures.html" target="_blank"><u>VIHE Audio Recordings</u></a> page to download our classes</strong>
</div>
<div class="green_left_nav_bottom_bg"> </div>

<div class="green_left_nav_top_bg"> </div>
<div class="green_left_nav_bg">
	<strong> <a href="/bhakti_pravesa.html" target="_blank"><u>Bhakti-pravesa 2023</u></a> </strong>
</div>
<div class="green_left_nav_bottom_bg"> </div> 

<div class="green_left_nav_top_bg"> </div>
<div class="green_left_nav_bg">
	<strong><a href="/bhakti_sastri.html" target="_blank"><u>Bhakti-sastri 2023</u></a></strong>
</div>
<div class="green_left_nav_bottom_bg"> </div>

<div class="green_left_nav_top_bg"> </div>
<div class="green_left_nav_bg">
	<strong><a href="https://play.google.com/store/apps/details?id=com.premabhakti.bhaktishastry" target="_blank"><u>Bhakti-sastri sloka app</u></a></strong>
</div>
<div class="green_left_nav_bottom_bg"> </div>

<div class="green_left_nav_top_bg"> </div>
<div class="green_left_nav_bg">
	<strong><a href="/sanskrit_pronunciation_seminar.html" target="_blank"><u>Sanskrit Pronunciation Course</u></a></strong>
</div>
<div class="green_left_nav_bottom_bg"> </div>

<!--
<div class="green_left_nav_top_bg"> </div>
<div class="green_left_nav_bg">
	<strong><a href="https://sanskritsense.thinkific.com/courses/complete-sanskrit-pronunciation?ref=6107c2" target="_blank"><u>Sanskrit Pronunciation Course</u></a></strong>
</div>
<div class="green_left_nav_bottom_bg"> </div>
-->

<div class="green_left_nav_top_bg"> </div>
<div class="green_left_nav_bg">
	<strong><a href="/service.html" target="_blank"><u>Service Opportunities at the VIHE</u></a></strong>
</div>
<div class="green_left_nav_bottom_bg"> </div>

