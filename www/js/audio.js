//document.querySelectorAll('.audio_track').addEventListener('click', function (e) {
$('a.audio_track').click(function (e) {
  console.log("id: " + this.id);
  console.log("href: " + this.href );

  extension = this.href.split('.').pop();
  console.log("extension: " + extension);
  if (extension == "mp3") {
    //add audio player to div.audio_div container
    var audio = $('<audio />', {
          autoPlay : 'false',
          controls : 'controls',
          type: 'audio/mp3',
          src: this.href
        });

    //$('<source />').attr('src', this.href).appendTo(audio);
    //var playerDIV = this.id+'_player';
    var playerDIV = this.id+'_audio';
    console.log('playerDIV: ' + playerDIV);
    audio.appendTo(playerDIV);
    $(playerDIV).show();

  }

  e.preventDefault();
});
