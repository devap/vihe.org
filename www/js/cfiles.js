var options = {};

$(document).ready(function(){


	$('.toplist').click(function() {
		var my_id = this.id;
		var my_name = my_id.substr(0,my_id.indexOf("_heading"));
		var my_path = my_name.replace("__", "/");
		var container = "#" + my_name;
		$(container).toggle("blind",options,500);
		return false;
	});

	$('.staticlist').click(function() {
		var my_id = this.id;
		var my_name = my_id.substr(0,my_id.indexOf("_heading"));
		var my_path = my_name.replace("__", "/");
		var container = "#" + my_name;
		$(container).toggle("blind",options,500);
		return false;
	});

	$('.sublist').click(function() {
		var my_id = this.id;
		var my_name = my_id.substr(0,my_id.indexOf("_heading"));
		var my_path = my_name.replace(/__/g, "/");
		my_path = my_path.replace(/SPACE/g, " ");
		my_path = my_path.replace(/DOT/g, ".");
		var container = "#" + my_name;
		$(container).toggle("blind",options,500);
		var my_path = my_path.replace("cc-", "");
		var my_url = "./DVDs/getlist.php?dir=" + my_path;
		console.log('my_path=' + my_path);
		console.log('my_url=' + my_url);
		$.ajax({
		  url: my_url,
		  cache: true,
		  success: function(html){
		    $(container).html(html);
		  }
		});

		if (my_id.indexOf("_nested") != -1) {
			$(container+"_nested").toggle("blind",options,500);
		}

		return false;
	});

 });
