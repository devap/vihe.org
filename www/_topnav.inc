﻿<!-- #BeginLibraryItem "/Library/top_nav_bar.lbi" -->
<div class="chromestyle" id="chromemenu">
    <ul>
        <li><a href="the_mission.html">THE MISSION</a></li>
        <li><a href="#" rel="correspcourses">CORRESPONDENCE COURSES</a></li>
        <li><a href="#" rel="viheretreats">RETREATS</a></li>             
        <li><a href="#" rel="ourannualcourses">OUR ANNUAL COURSES</a></li>
        <li><a href="#" rel="outstationcourses">OUTSTATION COURSES</a></li>
            </ul>
</div>
<!--1st drop down menu -->
<div style="height: auto; overflow: hidden; visibility: hidden; left: -1000px; top: -1000px;" id="correspcourses" class="dropmenudiv">
     <a href="correspondence.html">Bhakti-sastri (Correspondence)</a>
           </div>
<!--2nd drop down menu -->
<div style="height: auto; overflow: hidden; visibility: hidden; left: -1000px; top: -1000px;" id="viheretreats" class="dropmenudiv">
    <a href="/retreats/index.html" target="_blank">Govardhana & Holy Name</a> 
    <a href="/vaisnavi_retreat.html">Vaisnavi</a>
    <a href="/retreat_centre.html">Retreat Centre</a>
</div>
<!--3d drop down menu -->
<div id="ourannualcourses" class="dropmenudiv">

	<a href="/bhakti_pravesa.html">Bhakti-pravesa</a>
	<a href="/bhakti_sadacara.html">Bhakti-sadacara</a>
    <a href="/bhakti_sastri.html">Bhakti-sastri</a> 

<!--<a href="/bhagavad-vidya.html">Онлайн Бхакти-шастри</a>-->

	<a href="/bs_hindi.html">Bhakti-sastri (Hindi)</a>
    <a href="/bhakti_vaibhava.html">Bhakti-vaibhava (Online)</a>
    <a href="/bhakti_vedanta-online.html">Bhakti-vedanta (Online)</a>
    <a href="/bhakti_sarvabhauma.html">Bhakti-sarvabhauma</a>
	<a href="/sanskrit_pronunciation_seminar.html">Sanskrit Pronunciation Course</a>

    <a href="/ttc1.html">Teacher training course I</a>
    <a href="/ttc2.html">Teacher training course II</a>
    <a href="/ttc1-hindi.html">TTC I (Hindi)</a>
	
</div>

<!--4th drop down menu -->
<div id="outstationcourses" class="dropmenudiv">
    <a href="/world.html">Worldwide</a> 
    <a href="/india.html">India</a>
</div>



<!-- #EndLibraryItem -->
