var gulp = require('gulp');
var concat = require('gulp-concat');
var sass = require('gulp-sass');
var header = require('gulp-header');
var cleanCSS = require('gulp-clean-css');
var rename = require("gulp-rename");
var uglify = require('gulp-uglify');
var pkg = require('./package.json');
var browserSync = require('browser-sync').create();

// Set the banner content
var banner = ['/*!\n',
  ' * Start Bootstrap - <%= pkg.title %> v<%= pkg.version %> (<%= pkg.homepage %>)\n',
  ' * Copyright 2013-' + (new Date()).getFullYear(), ' <%= pkg.author %>\n',
  ' * Licensed under <%= pkg.license %> (https://github.com/BlackrockDigital/<%= pkg.name %>/blob/master/LICENSE)\n',
  ' */\n',
  ''
].join('');


// script paths
var jsDest = './js';

// move javascript files to js
gulp.task('vendor', () => {
    return gulp.src([
        'node_modules/jquery/dist/jquery.min.js',
        'node_modules/popper.js/dist/popper.min.js',
        'node_modules/bootstrap/dist/js/bootstrap.min.js'
    ])
    .pipe(concat('vendor.js'))
    .pipe(gulp.dest(jsDest))
    .pipe(browserSync.stream());
});

gulp.task('js', () => {
    return gulp.src([
        './src/js/retreats.js',
        './node_modules/nanogallery2/dist/jquery.nanogallery2.min.js',
        './src/js/nanogallery.govardhana.js',
        './src/js/nanogallery.japa.js'
     ])
    .pipe(concat('main.js'))
    .pipe(gulp.dest(jsDest))
    .pipe(rename('main.min.js'))
    .pipe(uglify())
    .pipe(gulp.dest(jsDest));
});

// convert scss files to css
gulp.task('sass', () => {
    return gulp.src([
        'node_modules/bootstrap/scss/bootstrap.scss',
        './src/scss/retreats.scss'
    ])
    .pipe(sass())
    .pipe(gulp.dest('./css'))
    .pipe(browserSync.stream());
});

// copy css files to css
gulp.task('css', () => {
    return gulp.src([
        'node_modules/nanogallery2/dist/css/font',
        'node_modules/nanogallery2/dist/css/nanogallery2.min.css'
    ])
    .pipe(gulp.dest('./css'));
});

// copy fonts files to css/fonts
gulp.task('font', () => {
    return gulp.src([
        'node_modules/nanogallery2/dist/css/font/*'
    ])
    .pipe(gulp.dest('./css/font'));
});

// Configure the browserSync task
gulp.task('serve', () => {
  browserSync.init({
    server: {
      baseDir: "./"
    }
  });
});

// default task
gulp.task('default', gulp.series('font', 'css', 'sass', 'vendor', 'js', () => {
    gulp.watch('./src/scss/*.scss', gulp.series('sass')),
    gulp.watch('./src/js/*.js', gulp.series('js'))
}));

// Dev task
gulp.task('dev', gulp.series('font', 'css', 'sass', 'vendor', 'js', 'serve', () => {
  gulp.watch('./src/scss/*.scss', ['sass']);
  gulp.watch('./src/js/*.js', ['js']);
  gulp.watch('./*.html', browserSync.reload);
}));
