const webpack = require('webpack');
const path = require('path');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = {
    entry: {
        vendor: "./src/js/_vendor.js",
        retreats: './src/js/_index.js',
        nanogallery2: './src/js/_nanogallery2.js',
    },
    plugins: [
        new webpack.ProvidePlugin({
           $: "jquery",
           jQuery: "jquery"
          }),
        new UglifyJsPlugin(),
        new MiniCssExtractPlugin({
              filename: '[name].css',
              chunkFilename: '[id].css',
              ignoreOrder: false, // Enable to remove warnings about conflicting order
            }),
    ],
	module: {
		rules: [
			{
                test: /\.js$/,
				include: [path.resolve(__dirname, 'src')],
				loader: 'babel-loader',
				options: {
					plugins: ['syntax-dynamic-import'],
					presets: [
						[
							'@babel/preset-env',
							{
								modules: false
							}
						]
					]
				}
			},
			{
				test: /\.(css|sass|scss)$/,
                use: [
                    {
                        loader: MiniCssExtractPlugin.loader
                    },
                    'css-loader',
                    'postcss-loader',
                    'sass-loader',
                ]
			},
            {
                test: /\.styl$/,
                use: [
                    {
                        loader: MiniCssExtractPlugin.loader
                    },
                    'css-loader',
                    'postcss-loader',
                    'stylus-loader'
                ]
            },
            {
                test: require.resolve('shifty'),
                use: [
                    { loader: 'expose-loader', options: 'NGTweenable' },
                ]
            },
            {
                test: require.resolve('hammerjs'),
                use: [
                    { loader: 'expose-loader', options: 'NGHammer' },
                ]
            },
            {
                test: require.resolve('imagesloaded'),
                use: [
                    { loader: 'expose-loader', options: 'ngimagesLoaded' },
                    { loader: 'expose-loader', options: 'ngImagesLoaded' },
                ]
            },
            {
                test: require.resolve('screenfull'),
                use: [
                    { loader: 'expose-loader', options: 'ngscreenfull' },
                ]
            },
            {
                test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                use: [
                    { loader: 'url-loader?limit=10000&mimetype=application/font-woff' }
                ]
            },
            {
                test: /\.(ttf|eot|otf|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                use: [
                    {
                        loader: 'file-loader'
                    }
                ]
            },
            {
                test: /\.(jpe?g|png|gif|mp3)$/i,
                use: [
                    {
                        loader: 'file-loader'
                    }
                ]
            },
		]
	},

	output: {
		//filename: '[name].[chunkhash].js',
        filename: '[name].js',
		path: path.resolve(__dirname, './dist')
	},

	optimization: {
        minimizer: [
              new UglifyJsPlugin({
                chunkFilter: (chunk) => {
                  // Exclude uglification for the `vendor` chunk
                  if (chunk.name === 'vendor') {
                    return false;
                  }

                  return true;
                },
              }),
            ],
		splitChunks: {
			cacheGroups: {
				vendors: {
					priority: -10,
					test: /[\\/]node_modules[\\/]/
				},
                /*
                styles: {
                    name: 'styles',
                    //test: /\.(css|sass|scss|styl)$/,
                    test: /\.(css)$/,
                    chunks: 'all',
                    enforce: true
                }
                */
			},

			chunks: 'async',
			minChunks: 1,
			minSize: 30000,
			name: true
		}
	}
};
