/*** nanogallery2 js ***/

global.jQuery = global.$ = require('jquery');
import 'shifty';
import 'hammerjs';
import 'imagesloaded';
import 'screenfull';
import 'nanogallery2/src/jquery.nanogallery2.core.js';
import 'nanogallery2/src/css/nanogallery2.css';
