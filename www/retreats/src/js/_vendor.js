/*** vendor js ***/

global.jQuery = global.$ = require('jquery');
import 'jquery';
import 'popper.js';
import 'bootstrap';

require('../scss/_bootstrap.scss');
require('open-iconic/font/css/open-iconic-bootstrap.css');
