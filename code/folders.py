def folders():
  """it is highly possible this function can be deleted, but we need to test"""
  _info_cnt = 0
  folders = defaultdict(lambda: defaultdict(list))
  for folder, entries in _folders.items():
    if 'desc' in entries:
      print ('do something with the [folder] desc you just found...')
    else:
      print('\nstart loop: for folder, entries in _folders.items(): ')
      print('\nfolder: ', folder, '\nentries: ', entries )
      #_obj = defaultdict(lambda: defaultdict(list))
      _obj = defaultdict(list)
      for section, info in entries.items():
        print('\nstart loop: for section, info in entries.items():')
        _info_cnt += 1
        print('\nfolder: ', folder, '\nsection:', section, '\ninfo: ', info, '\n')
        album = ''
        track = ''
        if 'desc' in info:
          print ('do something with the [section] desc you just found...')
        else:
          for group, albums in info.items():
            for k, v in albums.items():
              print ('start loop: for k, v in albums[0].items():\n')
              print('k: ', k, '\nv: ', v, '\n')
              if 'album' in k:
                album = v
              if 'track' in k:
                track = v
              if track:
                _obj[album].append(track)
                folders[folder][section] = _obj
              print ('end loop: for k, v in albums[0].items():')
      print('folder: ', folder, '\nsection: ', section, '\n album: ', album, '\n track: ', track)
      print('\nend loop: for section, info in entries.items():')
    print('\nend loop: for folder, entries in _folders.items(): ')
  print ('_info_cnt: ', _info_cnt)
