#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import re
import sys
from collections import defaultdict

import boto3

### S3
session = boto3.Session(profile_name='ICGAdmin')
s3r = session.resource('s3')
#s3c = boto3.client('s3')
s3c = session.client('s3')
bucket = 'vihe-media'
folder = 'audio'
delimiter = '/'
prefix = ''.join([folder, delimiter])

### If the Bucket has over 1,000 items, we need to use a Paginator.
# Paginator holds 1000 objects at a time and will continue to repeat in chunks of 1000.
paginator = s3c.get_paginator('list_objects_v2')
pages = paginator.paginate(Bucket=bucket, Prefix=folder)


def rec_dd():
  return defaultdict(rec_dd)


def remove_prefix(prefix, text):
  return text[text.startswith(prefix) and len(prefix):]


def get_description(key):
  full_key = ''.join([prefix, key])
  obj = s3r.Object(bucket, full_key)
  _description = obj.get()['Body'].read().decode('utf-8')
  return _description


def get_s3_keys(pager):
  """get list of all files in s3://bucket

  @parameters:
    pages: a boto3.resource paginator object

  returns:
    list = [
      'HG Dravida Prabhu/Ter Kadamba/DSC00231.JPG',
      'Retreats/The Vrindavana Experience/SNS-class.mp3',
      "Xtras/Bhaktivinode Thakur's bhajans/description.txt",
    ]
    Note: strips 'prefix' before returning key

  """
  _s3keys = []
  ignore_these = ['.DS_Store', '.gitignore']
  for page in pager:
    for obj in page['Contents']:
      key = remove_prefix(prefix, obj['Key'])
      if '.DS_Store' not in key and '.gitignore' not in key:
        _s3keys.append(key)
  print('S3 keys have been retrieved... (', len(_s3keys), 'of them in fact!)')
  return _s3keys


def parse_s3_keys(keys):
  """parse s3keys[] and convert to structured data

  s3keys is basically a list of strings containing S3 keys with '/' delimiters
  this functions parses each key and creates a large dict object for later use

  @parameters:
    list of keys

  returns:
    dict = {
    "folder":
      {"none":
        {"none":
          {"level3":
            {"tracks": [{"track": "file.ext", "uri": "/key/to/load/file"}] } } } } }
  """
  _folders = rec_dd()
  for key in keys:
    folder = ''
    level1 = ''
    level2 = ''
    level3 = ''
    track = ''
    desc = ''
    slashes = key.count('/')

    ### level3
    if slashes == 2:
      # 'HG Dravida Prabhu/Ter Kadamba/DSC00231.JPG'
      # 'Retreats/The Vrindavana Experience/SNS-class.mp3'
      # "Xtras/Bhaktivinode Thakur's bhajans/description.txt"
      # 'HG Adi Purusa Prabhu/NOD Overview Chapters 20-51/BV NOD 01.mp3'
      folder, level1, track = key.split('/')
      if track:
        if track == 'description.txt':
          _folders[folder][level1]["desc"] = get_description(key)
        else:
          _track_info = {
            "track": track,
            "uri": key
           }

          if not _folders[folder][level1]["tracks"]:
            _folders[folder][level1]["tracks"] = []
          _folders[folder][level1]["tracks"].append(_track_info)

    ### level1
    elif slashes == 3:
      # "Xtras/Bhaktivinode Thakur's bhajans/Saranagati/SHARANAGATI.DOC"
      # "Xtras/Bhaktivinode Thakur's bhajans/Gitavali/gitavali.RTF"
      folder, level1, level2, track = key.split('/')
      if track:
        if track == 'description.txt':
          _folders[folder][level1][level2]["desc"] = get_description(key)
        else:
          _track_info = {
            "track": track,
            "uri": key
           }

          if not _folders[folder][level1][level2]["tracks"]:
            _folders[folder][level1][level2]["tracks"] = []
          _folders[folder][level1][level2]["tracks"].append(_track_info)

    ### level1 with additional level2
    elif slashes == 4:
      # 'Xtras/Bhaktivinode Thakur's bhajans/Saranagati/07 Bhajana-Lalasa/02 hari he ! arthera.mp3'
      # 'Xtras/Bhaktivinode Thakur's bhajans/KalyanaKalpataru/02 Upadesha (Advice)/02 mana, tumi.mp3'
      # 'Xtras/Bhaktivinode Thakur's bhajans/Gitavali/12 Sri Siksastaka/04 prabhu (Git.,Siksastakam 4).mp3'
      folder, level1, level2, level3, track = key.split('/')
      if track:
        if track == 'description.txt':
          _folders[folder][level1][level2][level3]["desc"] = get_description(key)
        else:
          _track_info = {
            "track": track,
            "uri": key
           }

          if not _folders[folder][level1][level2][level3]["tracks"]:
            _folders[folder][level1][level2][level3]["tracks"] = []
          _folders[folder][level1][level2][level3]["tracks"].append(_track_info)

    # track directly in folder (no levels)
    elif slashes == 1:
      # 'Xtras/Krisna Das Babaji Maharaj (kirtan).mp3'
      # 'Retreats/description.txt'
      # 'Sounds of Vrindavana/VrindavanSounds.mp3'
      # 'Sounds of Vrindavana/description.txt'
      folder, track = key.split('/')
      if track:
        if track == 'description.txt':
          _folders[folder]["desc"] = get_description(key)
        else:
          _track_info = {
            "track": track,
            "uri": key
          }

          if not _folders[folder]["tracks"]:
            _folders[folder]["tracks"] = []
          _folders[folder]["tracks"].append(_track_info)

    else:
      # should not reach here, since there will always be a trailing '/'
      # and thus a minimum of 1 for key.count
      folder = 'ERROR'
      _obj = {
        "slashes": slashes,
        "key": key
       }
      _folders[folder] = _obj

  print('S3 data has been parsed...')
  return _folders


regex = re.compile('^(?:Dr\.\s|HG\s|HH\s|)(\w+)')


def get_sort_key(obj):
  _folder_name = None
  if type(obj) is dict:
    _folder_name = list(obj.keys())[0]
  elif type(obj) is list:
    _folder_name = obj[0]
  elif type(obj) is tuple:
    _folder_name = obj[0]
  elif type(obj) is str:
    _folder_name = obj
  else:
    sys.exit('get_sort_key() unknown type: ', type(obj))

  match = re.search(regex, _folder_name)
  return match.group(1)


def sort_folders(obj):
  _unsorted = []
  for folder, entries in obj.items():
    _folder = (folder, entries,)
    _unsorted.append(_folder)

  _sorted = sorted(_unsorted, key=get_sort_key)
  return _sorted
