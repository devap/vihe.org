#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
import copy
import json
import logging
import logging.config
import os
#import pickle
import shutil
import sys
import time
import uuid
from urllib.parse import quote, quote_plus

from debug2files import deBugData, deBugJSON
from jinja2 import Environment, FileSystemLoader, select_autoescape
from s3media import get_s3_keys, pages, parse_s3_keys, prefix, sort_folders

logging.config.fileConfig('logging.conf')
#logger = logging.getLogger("root")
logger = logging.getLogger("Adi")

parser = argparse.ArgumentParser()

env = Environment(
  loader=FileSystemLoader('templates', followlinks=True),
  autoescape=select_autoescape(['html', 'xml'])
)

#env = Environment(loader=FileSystemLoader('templates'))
#env = Environment(loader=FileSystemLoader('templates'), autoescape=True)

### media_url is used for generating the final URL to the object
media_uri = 'https://media.vihe.org/'
#media_uri = 'https://d1su4ik7pjeweo.cloudfront.net/'

ofile = '_s3_audio.inc'
timestr = time.strftime("%Y%m%d")
ofile_bak = ''.join([ofile, '.', timestr])

#parser.add_argument("-d", "--debug", dest="DEBUG", default=False, type=bool, action=argparse.BooleanOptionalAction, help="Debug")
parser.add_argument("-d", "--debug", dest="DEBUG", default=False, type=bool, help="Debug")
args = parser.parse_args()


def get_tracks(ary):
  if type(ary) != list:
    print('\nUNEXPECTED: type(ary): ', type(ary))
  _tracks = []
  for track in ary:
    iconClass = None
    trackLower = track["uri"].lower()
    if trackLower.endswith('.mp3') or trackLower.endswith('.wav'):
      iconClass = 'oi-play-circle'
    elif trackLower.endswith('.mp4') or trackLower.endswith('.mov'):
      iconClass = 'oi oi-video'
    elif trackLower.endswith('.jpg'):
      iconClass = 'oi-image'
    elif trackLower.endswith('.doc'):
      iconClass = 'oi-book'
    elif trackLower.endswith('.pdf'):
      iconClass = 'oi-document'
    elif trackLower.endswith('.rtf'):
      iconClass = 'oi-document'
    elif trackLower.endswith('.ttf'):
      iconClass = 'oi-text'
    elif trackLower.endswith('.zip'):
      iconClass = 'oi-envelope-closed'

    _url = ''.join([prefix, track["uri"]])
    url_encoded = ''.join([media_uri, quote_plus(_url)])
    _track_obj = {
      "track": track["track"],
      "url": url_encoded,
      "icon": iconClass
    }
    _tracks.append(_track_obj)

  return _tracks


def cleanup_schema(ary):
  _folders_clean = []

  for folder in ary:
    _folder_obj = {
      "name": folder[0],
      "level1": [],
      "tracks": []
    }

    # start level1 loop
    for level1_key, level1_data in folder[1].items():

      _level1_obj = {}

      if level1_key:
        if level1_key == 'desc' and level1_data:
          # this description is for the folder
          _folder_obj["desc"] = level1_data
        elif level1_key == 'tracks':
          # we have folder level tracks
          _folder_obj["tracks"] = get_tracks(level1_data)
        else:
          _level1_obj["name"] = level1_key

      if level1_key != 'desc' and level1_key != 'tracks':
        # start level2 loop/extraction
        for level2_key, level2_data in level1_data.items():
          _level2_obj = {}

          """
            level1_key:  Japa and Holy Name Retreat
            level2_key:  Japa Retreat 2007
            level2_data: {'tracks': [{'track': '01DAY1-class.mp3', 'uri': ''}, {}, {} ] }
            level1_key:  Bhaktivinode Thakur's bhajans
            level2_key:  Gitavali
            level3_key:  01 Arunodaya-Kirtan
            level3_tracks: [{}, {}, {}, ...]
            level2_key:  KalyanaKalpataru
            level2_data: {'01 Intro and Desc... ', {'tracks': [{'track': '', 'uri': ""}, {}, {} ] }
            level2_key:  Saranagati
            level2_data: {'0 Introduction', { 'tracks': [{},{},{}] } }
            level2_key:  desc
            level2_data: 'All of Bhaktivinode Thakur's Saranagati and Gitavali bhajans ...'
          """

          if level2_key:
            if level2_key == 'desc' and level2_data:
              _level1_obj["desc"] = level2_data
            elif level2_key == 'tracks':
              _level1_obj["tracks"] = get_tracks(level2_data)
            else:
              _level2_obj["name"] = level2_key

              # see if next level down contains tracks, or a level 3 album...
              for key, data in level2_data.items():
                if key == 'tracks':
                  _level2_obj["tracks"] = get_tracks(data)
                else:
                  _level3_obj = {
                    "name": key,
                    "tracks": get_tracks(data["tracks"])
                  }
                  if 'level3' not in _level2_obj:
                    _level2_obj["level3"] = []
                  _level2_obj["level3"].append(_level3_obj)

            if _level2_obj:
              if 'level2' not in _level1_obj:
                _level1_obj["level2"] = []
              _level1_obj["level2"].append(_level2_obj)

      #end of level2 loop
      if _level1_obj:
        _folder_obj["level1"].append(_level1_obj)

    _folders_clean.append(_folder_obj)

  _folders_all = {
    "folders": _folders_clean
  }

  return _folders_all


if __name__ == '__main__':

  logger = logging.getLogger("root")

  keys = get_s3_keys(pages)
  if args.DEBUG:
      deBugData(keys, '01_raw_s3_keys')

  data_raw = parse_s3_keys(keys)
  if args.DEBUG:
    deBugData(data_raw, '02_parsed_s3_keys')

  data_sorted = sort_folders(data_raw)
  if args.DEBUG:
    deBugData(data_sorted, '03_folders_sorted')

  data_clean = cleanup_schema(data_sorted)
  if args.DEBUG:
    deBugJSON(data_clean, '04_folders_cleaned')

  template = env.get_template('listings.html')
  markup = template.render(data_clean)

  shutil.copy(ofile, ofile_bak)
  with open(ofile, 'w') as fh:
    for line in markup:
      fh.write(line)

  #TODO: a flag for generating Aindra's tracks
  print("TODO: Need to a flag for generating Aindra's tracks?")

  sys.exit('Completed successfully!')
