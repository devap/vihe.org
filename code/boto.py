for bucket in s3.buckets.all():
  print(bucket.name)

for bucket in s3.buckets.all():
  for key in bucket.objects.all():
    print(key.key)


### List top-level common prefixes in Amazon S3 bucket
# This example shows how to list all of the top-level common prefixes in an Amazon S3 bucket:
import boto3

### what's the difference between these two?
s3_resource = boto3.resource('s3')
s3_client = boto3.client('s3')


bucket = 'vihemedia'
prefix = 'audio/'

client = boto3.client('s3')
paginator = client.get_paginator('list_objects_v2')
result = paginator.paginate(Bucket=bucket, Delimiter='/')
result = paginator.paginate(Bucket=bucket, Prefix=prefix, Delimiter='/')

for prefix in result.search('CommonPrefixes'):
  print(prefix.get('Prefix'))


# create the S3 resource, client, and get a listing of our buckets.
s3 = boto3.resource('s3')
s3client = boto3.client('s3')

response = s3client.list_buckets()
for bucket in response["Buckets"]:
  print(bucket['Name'])

# To list out the objects within a bucket, we can add the following:
theobjects = s3client.list_objects_v2(Bucket=bucket["Name"])
for object in theobjects["Contents"]:
  print(object["Key"])


### Now if the Bucket has over 1,000 items, the list_objects is limited to 1000 replies.
# To get around this, we need to use a Paginator.
import boto3

s3 = boto3.resource('s3')
s3client = boto3.client('s3')

response = s3client.list_buckets()
print(response)
for bucket in response["Buckets"]:
  # Create a paginator to pull 1000 objects at a time
  paginator = s3client.get_paginator('list_objects_v2')
  pageresponse = paginator.paginate(Bucket=bucket["Name"])

  # PageResponse Holds 1000 objects at a time and will continue to repeat in chunks of 1000.
  for pageobject in pageresponse:
    for object in pageobject["Contents"]:
      print(object["Key"])



### Restore Glacier objects in an Amazon S3 bucket
# The following example shows how to initiate restoration of glacier objects in an Amazon S3 bucket, determine if a restoration is on-going, and determine if a restoration is finished.
import boto3

s3 = boto3.resource('s3')
bucket = s3.Bucket('glacier-bucket')
for obj_sum in bucket.objects.all():
  obj = s3.Object(obj_sum.bucket_name, obj_sum.key)
  if obj.storage_class == 'GLACIER':
    # Try to restore the object if the storage class is glacier and
    # the object does not have a completed or ongoing restoration
    # request.
    if obj.restore is None:
      print('Submitting restoration request: %s' % obj.key)
      obj.restore_object()
    # Print out objects whose restoration is on-going
    elif 'ongoing-request="true"' in obj.restore:
      print('Restoration in-progress: %s' % obj.key)
    # Print out objects whose restoration is complete
    elif 'ongoing-request="false"' in obj.restore:
      print('Restoration complete: %s' % obj.key)




